<?php

use Illuminate\Database\Seeder;
use App\User;
use App\KartuKeluarga;
use App\LevelPengguna;

class UserTable extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      // Level Pengguna
      LevelPengguna::create([
        'nama' => 'Admin'
      ]);

      LevelPengguna::create([
        'nama' => 'RW'
      ]);

      LevelPengguna::create([
        'nama' => 'RT'
      ]);

      LevelPengguna::create([
        'nama' => 'Warga'
      ]);

      // akses admin id = 1
      User::create([
        'id' => '1122',
        'nama_lengkap' => 'Saya Admin Loh',
        'password' => bcrypt('admin'),
        'jenis_kelamin' => null,
        'tempat_lahir' => null,
        'tanggal_lahir' => null,
        'agama' => null,
        'pendidikan_terakhir' => null,
        'pekerjaan' => null,
        'golongan_darah' => null,
        'status_perkawinan' => null,
        'status_keluarga' => null,
        'wni' => null,
        'nama_ayah' => null,
        'nama_ibu' => null,
        'id_kartu_keluarga' => null,
        'id_level_pengguna' => 1,
        'aktif' => 1
      ]);

      // buat kartu keluarga
      KartuKeluarga::create([
        'id' => '3275111116070203',
        'alamat' => 'Jl RT dan RW sini',
        'rt' => '011',
        'rw' => '009',
        'kelurahan' => 'Kelurahan A',
        'kecamatan' => 'Kecamatan B',
        'kota' => 'Kota C',
        'provinsi' => 'Provinsi Z',
        'kode_pos' => '14045'
      ]);

      // buat data RT 3
      User::create([
        'id' => '3225110106070211',
        'nama_lengkap' => 'Bapak RT',
        'password' => bcrypt('3225110106070211'),
        'jenis_kelamin' => 1,
        'tempat_lahir' => 'Jakarta',
        'tanggal_lahir' => '19810101',
        'agama' => 'Islam',
        'pendidikan_terakhir' => 'SMA/SMK',
        'pekerjaan' => '-',
        'golongan_darah' => 'A',
        'status_perkawinan' => 1,
        'status_keluarga' => 'Kepala Keluarga',
        'wni' => '1',
        'nama_ayah' => 'Saya Papanya',
        'nama_ibu' => 'Saya Mamanya',
        'id_kartu_keluarga' => '3275111116070203',
        'id_level_pengguna' => 3,
        'aktif' => 1
      ]);

      // buat data RW 2
      User::create([
        'id' => '3225210206070211',
        'nama_lengkap' => 'Bapak RW',
        'password' => bcrypt('3225210206070211'),
        'jenis_kelamin' => 1,
        'tempat_lahir' => 'Jakarta',
        'tanggal_lahir' => '19810101',
        'agama' => 'Kristen',
        'pendidikan_terakhir' => 'SMA/SMK',
        'pekerjaan' => '-',
        'golongan_darah' => 'A',
        'status_perkawinan' => 1,
        'status_keluarga' => 'Anak',
        'wni' => '1',
        'nama_ayah' => 'Saya Papanya',
        'nama_ibu' => 'Saya Mamanya',
        'id_kartu_keluarga' => '3275111116070203',
        'id_level_pengguna' => 2,
        'aktif' => 1
      ]);
    }
}
