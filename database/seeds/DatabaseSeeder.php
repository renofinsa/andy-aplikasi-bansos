<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
      $this->call(JenisDanPengajuanTable::class);
      $this->call(UserTable::class);
      $this->call(DataWarga::class);
      $this->call(BeritaTable::class);
    }
}
