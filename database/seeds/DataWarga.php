<?php

use Illuminate\Database\Seeder;
use App\KartuKeluarga;
use App\User;

class DataWarga extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // buat kartu keluarga
        KartuKeluarga::create([
          'id' => '3275110106072021',
          'alamat' => 'Jl Permai Raya no 29',
          'rt' => '011',
          'rw' => '009',
          'kelurahan' => 'Kelurahan A',
          'kecamatan' => 'Kecamatan B',
          'kota' => 'Kota C',
          'provinsi' => 'Provinsi Z',
          'kode_pos' => '14045'
        ]);

        // buat data warga kepala keluarga
        User::create([
          'id' => '3275110189070211',
          'nama_lengkap' => 'Agus Yanto',
          'password' => bcrypt('3275110189070211'),
          'jenis_kelamin' => 1,
          'tempat_lahir' => 'Jakarta',
          'tanggal_lahir' => '19710101',
          'agama' => 'Islam',
          'pendidikan_terakhir' => 'SMA/SMK',
          'pekerjaan' => 'Pegawai Negeri',
          'golongan_darah' => 'AB',
          'status_perkawinan' => 1,
          'status_keluarga' => 'Kepala Keluarga',
          'wni' => '1',
          'nama_ayah' => 'Papa Yanto',
          'nama_ibu' => 'Ibu Yanto',
          'id_kartu_keluarga' => '3275110106072021',
          'id_level_pengguna' => 4,
          'aktif' => 0
        ]);

        // buat data warga istri
        User::create([
          'id' => '3275110189777211',
          'nama_lengkap' => 'Ayu Sriadi',
          'password' => bcrypt('3275110189777211'),
          'jenis_kelamin' => 1,
          'tempat_lahir' => 'Yogya',
          'tanggal_lahir' => '19730101',
          'agama' => 'Islam',
          'pendidikan_terakhir' => 'SMA/SMK',
          'pekerjaan' => 'Ibu Rumah Tangga',
          'golongan_darah' => 'A',
          'status_perkawinan' => 1,
          'status_keluarga' => 'Istri',
          'wni' => '1',
          'nama_ayah' => 'Papa Ayu',
          'nama_ibu' => 'Ibu Ayu',
          'id_kartu_keluarga' => '3275110106072021',
          'id_level_pengguna' => 4,
          'aktif' => 0
        ]);

        // buat data warga anak
        User::create([
          'id' => '3275110189887211',
          'nama_lengkap' => 'Agus Junior',
          'password' => bcrypt('3275110189887211'),
          'jenis_kelamin' => 1,
          'tempat_lahir' => 'Jakarta',
          'tanggal_lahir' => '20090201',
          'agama' => 'Islam',
          'pendidikan_terakhir' => 'SMA/SMK',
          'pekerjaan' => 'Pelajar',
          'golongan_darah' => 'A',
          'status_perkawinan' => 3,
          'status_keluarga' => 'Anak',
          'wni' => '1',
          'nama_ayah' => 'Agus Yanto',
          'nama_ibu' => 'Ayu Sriadi',
          'id_kartu_keluarga' => '3275110106072021',
          'id_level_pengguna' => 4,
          'aktif' => 0
        ]);
    }
}
