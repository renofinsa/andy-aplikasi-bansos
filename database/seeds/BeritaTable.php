<?php

use Illuminate\Database\Seeder;
use App\Berita;

class BeritaTable extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      Berita::create([
        'user_id' => 1,
        'status' => 1,
        'judul' => 'Cara Menggunakan Aplikasi',
        'deskripsi' => 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Beatae repellat, expedita nostrum perferendis maxime accusamus est pariatur a molestias voluptatum ex necessitatibus asperiores molestiae suscipit et possimus? Illum sunt quasi eveniet quo suscipit nobis non perspiciatis, nemo quia corrupti dolore ea quidem minus dignissimos reprehenderit sequi aut, delectus esse hic.',
        'tanggal_buat' => date('Y-m-d H:i:s')
      ]);
    }
}


