<?php

use Illuminate\Database\Seeder;
use App\JenisPengajuan;

class JenisDanPengajuanTable extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Jenis Pengajuan
        JenisPengajuan::create([
          'nama' => 'Pengajuan Pembuatan KTP',
          'tipe' => 1
        ]);

        JenisPengajuan::create([
          'nama' => 'Pengajuan Pembuatan KK',
          'tipe' => 1
        ]);

        JenisPengajuan::create([
          'nama' => 'Domisili',
          'tipe' => 1
        ]);

        JenisPengajuan::create([
          'nama' => 'Akte Kelahiran',
          'tipe' => 1
        ]);

        JenisPengajuan::create([
          'nama' => 'Akte Kematian',
          'tipe' => 1
        ]);

        JenisPengajuan::create([ // hanya dapat di ambil sekali
          'nama' => 'PKH (Program Keluarga Harapan)',
          'tipe' => 2
        ]);

        JenisPengajuan::create([
          'nama' => 'Lansia',
          'tipe' => 2
        ]);

        JenisPengajuan::create([
          'nama' => 'BPNT (Bantuan Pangan Non Tunai)',
          'tipe' => 2
        ]);

        JenisPengajuan::create([
          'nama' => 'BLT (Bantuan Langsung Tunai)',
          'tipe' => 2
        ]);
        // End Jenis Pengajuan
    }
}
