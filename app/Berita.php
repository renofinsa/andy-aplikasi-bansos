<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Berita extends Model
{
    protected $table = 'berita';

    protected $fillable = [
        'id',
        'user_id',
        'status',
        'judul',
        'deskripsi',
        'tanggal_buat'
    ];

    public $timestamps = false;
    
    public function author(){
      return $this->belongsTo(User::class, 'user_id');
    }
}
