<?php

namespace App\Http\Middleware;

use Closure;

class isWarga
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
      if (auth()->user()->id_level_pengguna == 4) {
        return $next($request);
      }

      return redirect('/404');
    }
}
