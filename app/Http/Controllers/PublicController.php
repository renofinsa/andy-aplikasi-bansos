<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;

class PublicController extends Controller
{
  public function login()
  {
    return view('pages.login');
  }

  public function actived()
  {
    return view('pages.aktivasi-akun');
  }

  public function activedStore(Request $request)
  {
    // VALIDATION
    $get = $request->validate([
      'id' => ['required', 'max:16', 'min:16']
    ]);

    // INSERT
    $data = User::find($get['id']);
    
    if (!$data) {
        return redirect()->back()->with('alert', 'Data tidak ditemukan.');
    }

    $data->aktif = 1;
    $data->save();

    // RESPONSES
    return redirect('/')->with('alert', 'Akun berhasil diaktifkan.');
  }


}
