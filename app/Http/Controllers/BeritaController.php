<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Berita;
use Auth;

class BeritaController extends Controller
{
  public function news()
  {
    $data = Berita::all();
    return view('pages.news', compact('data'));
  }

  public function newsCreate()
  {
    return view('pages.news-create');
  }

  public function newsStore(Request $request)
  {
    // VALIDATION
    $get = $request->validate([
      'judul' => ['required'],
      'deskripsi' => ['required'],
    ]);

    // INSERT
    $data = new Berita;
    $data->user_id = Auth::user()->id;
    $data->judul = $get['judul'];
    $data->deskripsi = $get['deskripsi'];
    $data->tanggal_buat = date('Y-m-d');
    $data->save();

    // RESPONSES
    if(Auth::user()->id_level_pengguna == 1) {
      return redirect(route('admin-news'))->with('alert', 'Berita berhasil dibuat.');
    } else if (Auth::user()->id_level_pengguna == 2) {
      return redirect(route('rw-news'))->with('alert', 'Berita berhasil dibuat.');
    } else if (Auth::user()->id_level_pengguna == 1) {
      return redirect(route('rt-news'))->with('alert', 'Berita berhasil dibuat.');
    } else {
      return redirect()->back()->with('alert', 'Berita berhasil dibuat.');
    }
  }

  public function newsEdit($id)
  {
    $data = Berita::find($id);
    return view('pages.news-edit', compact('data'));
  }

  public function newsUpdate(Request $request, $id)
  {
    // VALIDATION
    $get = $request->validate([
      'judul' => ['required'],
      'deskripsi' => ['required'],
    ]);

    // INSERT
    $data = Berita::find($id);
    $data->user_id = Auth::user()->id;
    $data->judul = $get['judul'];
    $data->deskripsi = $get['deskripsi'];
    $data->tanggal_buat = date('Y-m-d');
    $data->save();

    // RESPONSES
    if(Auth::user()->id_level_pengguna == 1) {
      return redirect(route('admin-news'))->with('alert', 'Berita berhasil dibuat.');
    } else if (Auth::user()->id_level_pengguna == 2) {
      return redirect(route('rw-news'))->with('alert', 'Berita berhasil dibuat.');
    } else if (Auth::user()->id_level_pengguna == 1) {
      return redirect(route('rt-news'))->with('alert', 'Berita berhasil dibuat.');
    } else {
      return redirect()->back()->with('alert', 'Berita berhasil dibuat.');
    }
  }

  public function newsDestroy($id)
  {
    $data = Berita::find($id);
    $data->delete();

    // RESPONSES
    if(Auth::user()->id_level_pengguna == 1) {
      return redirect(route('admin-news'))->with('alert', 'Berita berhasil dihapus.');
    } else if (Auth::user()->id_level_pengguna == 2) {
      return redirect(route('rw-news'))->with('alert', 'Berita berhasil dihapus.');
    } else if (Auth::user()->id_level_pengguna == 1) {
      return redirect(route('rt-news'))->with('alert', 'Berita berhasil dihapus.');
    } else {
      return redirect()->back()->with('alert', 'Berita berhasil dihapus.');
    }
  }
}
