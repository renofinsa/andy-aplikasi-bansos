<?php

namespace App\Http\Controllers\Auth;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Providers\RouteServiceProvider;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Auth;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    // protected $redirectTo = RouteServiceProvider::HOME;
    protected function redirectTo () 
    {
      try {
        if (Auth::user()->id_level_pengguna == 1) {
          // as admin
          return '/admin';
        } else if (Auth::user()->id_level_pengguna == 2) {
          // as RW
          return '/rw';
        } else if (Auth::user()->id_level_pengguna == 3) {
          // as RT
          return '/rt';
        } else if (Auth::user()->id_level_pengguna == 4) {
          return '/warga';
        } else {
          // if id_level pengguna not found!
          return '/logout';
        }
      } catch (\Throwable $th) {
        // throw $th;
        return '/logout';
      }
    }

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    public function logout(Request $request) {
      $request->session()->flush();
      Auth::logout();
      return redirect('/');
    }
}
