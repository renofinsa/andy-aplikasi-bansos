<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Pengajuan;
use App\JenisPengajuan;
use App\Berita;
use App\KartuKeluarga;
use App\User;
use Auth;

class WargaController extends Controller
{
  public function index()
  {
    $data = Berita::orderBy('tanggal_buat', 'desc')->get();
    return view('pages.dashboard', compact('data'));
  }

  public function pengajuan()
  {
    $data = Pengajuan::where('id_warga', Auth::user()->id)->get();
    return view('pages.warga-pengajuan', compact('data'));
  }

  public function pengajuanCreateSuratPengantar()
  {
    $data = JenisPengajuan::where('tipe', 'surat pengantar')->get();
    $head = 'Surat Pengantar';
    return view('pages.warga-pengajuan-create', compact('data', 'head'));
  }
  
  public function pengajuanCreateSuratKeteranganTidakMampu()
  {
    $data = JenisPengajuan::where('tipe', 'surat keterangan tidak mampu')->get();
    $head = 'Surat Keterangan Tidak Mampu';
    return view('pages.warga-pengajuan-create', compact('data', 'head'));
  }


  public function pengajuanStore(Request $request)
  {
    // VALIDATION
    $get = $request->validate([
      'id_jenis_pengajuan' => ['required'],
      'pesan' => ['required'],
    ]);

    // CHECKED
    $data_kk = User::where('id_kartu_keluarga', Auth::user()->id_kartu_keluarga)->get();

    $load_data = array();
    for ($i=0; $i < count($data_kk); $i++) { 
      $get_bansos = Pengajuan::where([['id_warga', $data_kk[$i]->id], ['id_jenis_pengajuan', 6]])->whereIn('status', ['diterima', 'baru'])->get();
      if (count($get_bansos) !== 0) {
        array_push($load_data, $get_bansos);
      }
    }

    // IF AVAILABLE
    if (count($load_data) !== 0 ) {
      return redirect()->back()->with('alert', 'PKH tidak dapat diambil, karena salah satu anggota keluarga kamu sudah mengajukan PKH.');
    }

    // INSERT
    $data = new Pengajuan;
    $data->id_jenis_pengajuan = $get['id_jenis_pengajuan'];
    $data->id_warga = Auth::user()->id;
    $data->pesan = $get['pesan'];
    $data->status = 1;
    $data->tanggal_pengajuan = date('Y-m-d');
    $data->save();

    // RESPONSES
    if(Auth::user()->id_level_pengguna == 4) {
      return redirect(route('warga-pengajuan'))->with('alert', 'Pengajuan dikirim.');
    } else if (Auth::user()->id_level_pengguna == 2) {
      return redirect(route('rw-pengajuan'))->with('alert', 'Pengajuan dikirim.');
    } else {
      return redirect(route('rt-pengajuan'))->with('alert', 'Pengajuan dikirim.');
    }
  }
  
  // RT & RW
  public function tanggapiPengajuan()
  {
    $data = Pengajuan::all();
    return view('pages.warga-tanggapi-pengajuan', compact('data'));
  }

  // RW
  public function pengajuanAction(Request $request)
  {
    $data = Pengajuan::find($request->get('id'));
    $data->status = $request->get('status');
    $data->save();
    // RESPONSES
    if(Auth::user()->id_level_pengguna == 3) {
      return redirect(route('rt-tanggapi-pengajuan'))->with('alert', 'Pengajuan berhasil ditanggapi.');
    } else if (Auth::user()->id_level_pengguna == 2) {
      return redirect(route('rw-tanggapi-pengajuan'))->with('alert', 'Pengajuan berhasil ditanggapi.');
    } else {
      return redirect()->back();
    }
  }
}
