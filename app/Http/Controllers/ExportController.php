<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Pengajuan;
use App\User;
use PDF;

class ExportController extends Controller
{
  public function exportDataPengajuan()
  {
    $data = Pengajuan::orderBy('tanggal_pengajuan', 'desc')->get();
    $pdf = PDF::loadview('themes.data-pengajuan', compact('data'))->setPaper('a4', 'landscape');
    return $pdf->stream('laporan-data-pengajuan-pdf');
  }

  public function exportSuratPengantar($id)
  {
    $data = Pengajuan::find($id);
    $user = User::find($data->id_warga);
    $rt = User::where('id_level_pengguna', 3)->first();
    $rw = User::where('id_level_pengguna', 2)->first();
    $pdf = PDF::loadview('themes.surat-pengantar', compact('data', 'user', 'rt', 'rw'))->setPaper('a4', 'portrait');
    return $pdf->stream('laporan-surat-pengantar-pdf');
  }
}
