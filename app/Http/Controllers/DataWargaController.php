<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\KartuKeluarga;
use App\Pengajuan;
use App\User;
use Auth;

class DataWargaController extends Controller
{
  public function kk()
  {
    $data = KartuKeluarga::all();
    return view('pages.kk', compact('data'));
  }

  public function formKK()
  {
    return view('pages.kk-create');
  }

  public function storeKK(Request $request)
  {
    // VALIDATION
    $get = $request->validate([
      'id' => ['required', 'unique:users', 'max:16', 'min:16'],
      'alamat' => ['required'],
      'rt' => ['required', 'min:3', 'max:3'],
      'rw' => ['required', 'min:3', 'max:3'],
      'kelurahan' => ['required'],
      'kecamatan' => ['required'],
      'kota' => ['required'],
      'provinsi' => ['required'],
      'kode_pos' => ['required', 'min:5', 'max:5'],
    ]);

    // INSERT DATA
    $data = new KartuKeluarga();
    $data->id = $get['id'];
    $data->alamat = $get['alamat'];
    $data->rt = $get['rt'];
    $data->rw = $get['rw'];
    $data->kelurahan = $get['kelurahan'];
    $data->kecamatan = $get['kecamatan'];
    $data->kota = $get['kota'];
    $data->provinsi = $get['provinsi'];
    $data->kode_pos = $get['kode_pos'];
    $data->save();

    // RESPONSES
    if (Auth::user()->id_level_pengguna == 1) {
      return redirect(route('admin-show-kk', $data->id))->with('alert', 'Kartu keluarga berhasil ditambah.');
    } else if (Auth::user()->id_level_pengguna == 2) {
      return redirect(route('rw-show-kk', $data->id))->with('alert', 'Kartu keluarga berhasil ditambah.');
    } else {
      return redirect(route('rt-show-kk', $data->id))->with('alert', 'Kartu keluarga berhasil ditambah.');
    }
  }

  public function showKK($id)
  {
    $data = User::where([['id_kartu_keluarga', $id], ['id_level_pengguna', '!=', 1]])->get();
    $id = KartuKeluarga::find($id);
    return view('pages.warga', compact('data', 'id'));
  }

  public function editKK($id)
  {
    $data = KartuKeluarga::find($id);
    return view('pages.kk-edit', compact('data'));
  }

  public function updateKK(Request $request, $id)
  {
    // VALIDATION
    $get = $request->validate([
      'alamat' => ['required'],
      'rt' => ['required', 'min:3', 'max:3'],
      'rw' => ['required', 'min:3', 'max:3'],
      'kelurahan' => ['required'],
      'kecamatan' => ['required'],
      'kota' => ['required'],
      'provinsi' => ['required'],
      'kode_pos' => ['required', 'min:5', 'max:5'],
    ]);

    // INSERT DATA
    $data = KartuKeluarga::find($id);
    $data->alamat = $get['alamat'];
    $data->rt = $get['rt'];
    $data->rw = $get['rw'];
    $data->kelurahan = $get['kelurahan'];
    $data->kecamatan = $get['kecamatan'];
    $data->kota = $get['kota'];
    $data->provinsi = $get['provinsi'];
    $data->kode_pos = $get['kode_pos'];
    $data->save();

    // RESPONSES
    if (Auth::user()->id_level_pengguna == 1) {
      return redirect(route('admin-kk'))->with('alert', 'Kartu keluarga berhasil diubah.');
    } else if (Auth::user()->id_level_pengguna == 2) {
      return redirect(route('rw-kk'))->with('alert', 'Kartu keluarga berhasil diubah.');
    } else {
      return redirect(route('rt-kk'))->with('alert', 'Kartu keluarga berhasil diubah.');
    }
  }

  public function deleteKK($id)
  {
    $kk = KartuKeluarga::find($id);
    // dd(count($kk->anggota));
    if (count($kk->anggota) !== 0) {
        for ($i=0; $i < count($kk->anggota); $i++) { 
          $user = User::find($kk->anggota[$i]->id);
          $data = Pengajuan::where('id_warga', $user->id)->get();
          if (count($data) !== 0) {
            for ($i=0; $i < count($data); $i++) { 
              $select = Pengajuan::find($data[$i]->id);
              $select->delete();
            }
          }
          $user->delete();
        }
    }
    $kk->delete();

    // RESPONSES
    if (Auth::user()->id_level_pengguna == 1) {
      return redirect()->back()->with('alert', 'Data warga berhasil dihapus.');
    } else if (Auth::user()->id_level_pengguna == 2) {
      return redirect()->back()->with('alert', 'Data warga berhasil dihapus.');
    } else {
      return redirect()->back()->with('alert', 'Data warga berhasil dihapus.');
    }
  }

  public function formDataWarga($id)
  {
    $data = KartuKeluarga::find($id);
    return view('pages.warga-create', compact('data'));
  }

  public function storeDataWarga(Request $request, $id)
  {
    // VALIDATION
    $get = $request->validate([
      'id' => ['required', 'unique:users', 'max:16', 'min:16'],
      'nama_lengkap' => ['required'],
      'jenis_kelamin' => ['required'],
      'tempat_lahir' => ['required'],
      'tanggal_lahir' => ['required'],
      'agama' => ['required'],
      'pendidikan_terakhir' => ['required'],
      'pekerjaan' => ['required'],
      'golongan_darah' => ['required'],
      'status_perkawinan' => ['required'],
      'status_keluarga' => ['required'],
      'wni' => ['required'],
      'nama_ayah' => ['required'],
      'nama_ibu' => ['required'],
      'id_level_pengguna' => ['required']
    ]);

    // INSERT DATA
    $data = new User;
    $data->id = $get['id'];
    $data->nama_lengkap = $get['nama_lengkap'];
    $data->password = bcrypt($get['id']);
    $data->jenis_kelamin = $get['jenis_kelamin'];
    $data->tempat_lahir = $get['tempat_lahir'];
    $data->tanggal_lahir = $get['tanggal_lahir'];
    $data->agama = $get['agama'];
    $data->pendidikan_terakhir = $get['pendidikan_terakhir'];
    $data->pekerjaan = $get['pekerjaan'];
    $data->golongan_darah = $get['golongan_darah'];
    $data->status_perkawinan = $get['status_perkawinan'];
    $data->status_keluarga = $get['status_keluarga'];
    $data->wni = $get['wni'];
    $data->nama_ayah = $get['nama_ayah'];
    $data->nama_ibu = $get['nama_ibu'];
    $data->id_kartu_keluarga = $id;
    $data->id_level_pengguna = $get['id_level_pengguna'];
    $data->aktif = 0;
    $data->save();

    // RESPONSES
    if (Auth::user()->id_level_pengguna == 1) {
      return redirect(route('admin-show-kk', $id))->with('alert', 'Warga baru berhasil dibuat.');
    } else if (Auth::user()->id_level_pengguna == 2) {
      return redirect(route('rw-show-kk', $id))->with('alert', 'Warga baru berhasil dibuat.');
    } else {
      return redirect(route('rt-show-kk', $id))->with('alert', 'Warga baru berhasil dibuat.');
    }
  }

  public function editDataWarga($id){
    $data = User::find($id);
    return view('pages.warga-edit', compact('data'));
  }

  public function updateDataWarga(Request $request, $id){
    // VALIDATION
    $get = $request->validate([
      'nama_lengkap' => ['required'],
      'jenis_kelamin' => ['required'],
      'tempat_lahir' => ['required'],
      'tanggal_lahir' => ['required'],
      'agama' => ['required'],
      'pendidikan_terakhir' => ['required'],
      'pekerjaan' => ['required'],
      'golongan_darah' => ['required'],
      'status_perkawinan' => ['required'],
      'status_keluarga' => ['required'],
      'wni' => ['required'],
      'nama_ayah' => ['required'],
      'nama_ibu' => ['required'],
      'id_level_pengguna' => ['required']
    ]);

    // INSERT DATA
    $data = User::find($id);
    $data->nama_lengkap = $get['nama_lengkap'];
    $data->jenis_kelamin = $get['jenis_kelamin'];
    $data->tempat_lahir = $get['tempat_lahir'];
    $data->tanggal_lahir = $get['tanggal_lahir'];
    $data->agama = $get['agama'];
    $data->pendidikan_terakhir = $get['pendidikan_terakhir'];
    $data->pekerjaan = $get['pekerjaan'];
    $data->golongan_darah = $get['golongan_darah'];
    $data->status_perkawinan = $get['status_perkawinan'];
    $data->status_keluarga = $get['status_keluarga'];
    $data->wni = $get['wni'];
    $data->nama_ayah = $get['nama_ayah'];
    $data->nama_ibu = $get['nama_ibu'];
    $data->id_level_pengguna = $get['id_level_pengguna'];
    $data->aktif = 0;
    $data->save();

    // RESPONSES
    if (Auth::user()->id_level_pengguna == 1) {
      return redirect(route('admin-show-kk', $data->id_kartu_keluarga))->with('alert', 'Data warga berhasil diubah.');
    } else if (Auth::user()->id_level_pengguna == 2) {
      return redirect(route('rw-show-kk', $data->id_kartu_keluarga))->with('alert', 'Data warga berhasil diubah.');
    } else {
      return redirect(route('rt-show-kk', $data->id_kartu_keluarga))->with('alert', 'Data warga berhasil diubah.');
    }
  }

  public function dataWarga()
  {
    $data = User::where('id_level_pengguna', '!=', 1)->get();
    return view('pages.warga-list', compact('data'));
  }

  public function deleteDataWarga($id)
  {
    $user = User::find($id);
    $data = Pengajuan::where('id_warga', $user->id)->get();
    if (count($data) !== 0) {
      for ($i=0; $i < count($data); $i++) { 
        $select = Pengajuan::find($data[$i]->id);
        $select->delete();
      }
    }
    $user->delete();

    // RESPONSES
    if (Auth::user()->id_level_pengguna == 1) {
      return redirect()->back()->with('alert', 'Data warga berhasil dihapus.');
    } else if (Auth::user()->id_level_pengguna == 2) {
      return redirect()->back()->with('alert', 'Data warga berhasil dihapus.');
    } else {
      return redirect()->back()->with('alert', 'Data warga berhasil dihapus.');
    }
  }
}
