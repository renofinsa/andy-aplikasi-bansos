<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\KartuKeluarga;
use App\User;
use App\Berita;
use Auth;

class AdminController extends Controller
{
  public function __construct()
  {
    $this->middleware('is_admin');
  }

  public function index()
  {
    $data = Berita::orderBy('tanggal_buat', 'desc')->get();
    // return view('layouts.master');
    return view('pages.dashboard', compact('data'));
  } 

  
}
