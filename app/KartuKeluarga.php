<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class KartuKeluarga extends Model
{
  protected $table = 'kartu_keluarga';

  protected $fillable = [
    'id',
    'alamat',
    'rt',
    'rw',
    'kelurahan',
    'kecamatan',
    'kota',
    'provinsi',
    'kode_pos'
  ];

  public $timestamps = false;
  public $incrementing = false;

  public function anggota(){
    return $this->hasMany(User::class,'id_kartu_keluarga');
  }
  
  public function kepala($id) {
    $data = User::where([['id_kartu_keluarga', $id], ['status_keluarga', 'Kepala Keluarga']])->first();
    return $data;
  }
}
