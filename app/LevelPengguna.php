<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class LevelPengguna extends Model
{
    protected $table = 'level_pengguna';

    protected $fillable = [
        'id',
        'nama'
    ];

    public $timestamps = false;
}
