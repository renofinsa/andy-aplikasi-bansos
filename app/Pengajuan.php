<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Pengajuan extends Model
{
    protected $table = 'pengajuan';

    protected $fillable = [
        'id',
        'id_jenis_pengajuan',
        'id_warga',
        'status',
        'pesan',
        'tanggal_pengajuan'
    ];

    public $timestamps = false;
    
    public function jenis(){
      return $this->belongsTo(JenisPengajuan::class,'id_jenis_pengajuan');
    }
    
    public function user(){
      return $this->belongsTo(User::class,'id_warga');
    }
}
