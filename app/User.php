<?php

namespace App;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */

    protected $table = 'users';

    protected $fillable = [
        'id', 'nama_lengkap', 'password', 'jenis_kelamin', 'tempat_lahir', 'tanggal_lahir', 'agama', 'pendidikan_terakhir', 'pekerjaan', 'golongan_darah', 'status_perkawinan', 'status_keluarga', 'wni', 'nama_ayah', 'nama_ibu', 'id_kartu_keluarga', 'id_level_pengguna', 'aktif'
    ];

    public $timestamps = false;
    public $incrementing = false;

    public function kk($id){
      $data = KartuKeluarga::where('id', $id)->first();
      return $data;
    }

    public function level($id)
    {
      return LevelPengguna::find($id);
    }
    // return $this->belongsTo('App\User', 'foreign_key', 'other_key');
    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password'
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];
}
