<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/download/data-pengajuan', 'ExportController@exportDataPengajuan')->name('export-data-pengajuan');
Route::get('/download/surat-pengantar/{id}', 'ExportController@exportSuratPengantar')->name('export-surat-pengantar');
Route::get('/home', 'HomeController@index')->name('home');
Route::get('/logout', '\App\Http\Controllers\Auth\LoginController@logout')->name('user-logout');
// public controller
Route::get('/', 'PublicController@login')->name('get-login');
Route::get('/aktivasi', 'PublicController@actived')->name('get-actived');
Route::post('/', 'PublicController@activedStore')->name('post-actived-store');


// is_rt
Route::group(['middleware' => ['is_rt'], 'prefix' => 'rt'], function () {
  Route::get('/', 'WargaController@index')->name('rt');
  Route::get('/pengajuan', 'WargaController@pengajuan')->name('rt-pengajuan');
  Route::get('/tanggapi-pengajuan', 'WargaController@tanggapiPengajuan')->name('rt-tanggapi-pengajuan');
  Route::get('/pengajuan/surat-pengantar', 'WargaController@pengajuanCreateSuratPengantar')->name('rt-pengajuan-surat-pengantar');
  Route::get('/pengajuan/surat-tidak-mampu', 'WargaController@pengajuanCreateSuratKeteranganTidakMampu')->name('rt-pengajuan-surat-keterangan-tidak-mampu');
  Route::post('/pengajuan/store', 'WargaController@pengajuanStore')->name('rt-pengajuan-store');
  Route::post('/pengajuan/action', 'WargaController@pengajuanAction')->name('rt-pengajuan-action');
  // News
  Route::get('/news', 'BeritaController@news')->name('rt-news');
  Route::get('/news/baru', 'BeritaController@newsCreate')->name('rt-news-create');
  Route::post('/news/store', 'BeritaController@newsStore')->name('rt-news-store');
  Route::get('/news/edit/{id}', 'BeritaController@newsEdit')->name('rt-news-edit');
  Route::patch('/news/update/{id}', 'BeritaController@newsUpdate')->name('rt-news-update');
  Route::get('/news/delete/{id}', 'BeritaController@newsDestroy')->name('rt-news-destroy');
  // KK
  Route::get('/kk', 'DataWargaController@kk')->name('rt-kk');
  Route::get('/kk/baru', 'DataWargaController@formKK')->name('rt-form-kk');
  Route::post('/kk/store', 'DataWargaController@storeKK')->name('rt-store-kk');
  Route::get('/kk/edit/{id}', 'DataWargaController@editKK')->name('rt-edit-kk');
  Route::patch('/kk/update/{id}', 'DataWargaController@updateKK')->name('rt-update-kk');
  Route::get('/kk/delete/{id}', 'DataWargaController@deleteKK')->name('rt-delete-kk');
  Route::get('/kk/show/{id}', 'DataWargaController@showKK')->name('rt-show-kk');
  // Warga
  Route::get('/data-warga', 'DataWargaController@dataWarga')->name('rt-datawarga');
  Route::get('/data-warga/{id}/baru', 'DataWargaController@formDataWarga')->name('rt-form-datawarga');
  Route::post('/data-warga/{id}/store', 'DataWargaController@storeDataWarga')->name('rt-store-datawarga');
  Route::get('/data-warga/edit/{id}', 'DataWargaController@editDataWarga')->name('rt-edit-datawarga');
  Route::patch('/data-warga/update/{id}', 'DataWargaController@updateDataWarga')->name('rt-update-datawarga');
  Route::get('/data-warga/delete/{id}', 'DataWargaController@deleteDataWarga')->name('rt-delete-datawarga');
});

// is_rw
Route::group(['middleware' => ['is_rw'], 'prefix' => 'rw'], function () {
  Route::get('/', 'WargaController@index')->name('rw');
  Route::get('/pengajuan', 'WargaController@pengajuan')->name('rw-pengajuan');
  Route::get('/tanggapi-pengajuan', 'WargaController@tanggapiPengajuan')->name('rw-tanggapi-pengajuan');
  Route::get('/pengajuan/surat-pengantar', 'WargaController@pengajuanCreateSuratPengantar')->name('rw-pengajuan-surat-pengantar');
  Route::get('/pengajuan/surat-tidak-mampu', 'WargaController@pengajuanCreateSuratKeteranganTidakMampu')->name('rw-pengajuan-surat-keterangan-tidak-mampu');
  Route::post('/pengajuan/store', 'WargaController@pengajuanStore')->name('rw-pengajuan-store');
  Route::post('/pengajuan/action', 'WargaController@pengajuanAction')->name('rw-pengajuan-action');
  // News
  Route::get('/news', 'BeritaController@news')->name('rw-news');
  Route::get('/news/baru', 'BeritaController@newsCreate')->name('rw-news-create');
  Route::post('/news/store', 'BeritaController@newsStore')->name('rw-news-store');
  Route::get('/news/edit/{id}', 'BeritaController@newsEdit')->name('rw-news-edit');
  Route::patch('/news/update/{id}', 'BeritaController@newsUpdate')->name('rw-news-update');
  Route::get('/news/delete/{id}', 'BeritaController@newsDestroy')->name('rw-news-destroy');
  // KK
  Route::get('/kk', 'DataWargaController@kk')->name('rw-kk');
  Route::get('/kk/baru', 'DataWargaController@formKK')->name('rw-form-kk');
  Route::post('/kk/store', 'DataWargaController@storeKK')->name('rw-store-kk');
  Route::get('/kk/edit/{id}', 'DataWargaController@editKK')->name('rw-edit-kk');
  Route::patch('/kk/update/{id}', 'DataWargaController@updateKK')->name('rw-update-kk');
  Route::get('/kk/delete/{id}', 'DataWargaController@deleteKK')->name('rw-delete-kk');
  Route::get('/kk/show/{id}', 'DataWargaController@showKK')->name('rw-show-kk');
  // Warga
  Route::get('/data-warga', 'DataWargaController@dataWarga')->name('rw-datawarga');
  Route::get('/data-warga/{id}/baru', 'DataWargaController@formDataWarga')->name('rw-form-datawarga');
  Route::post('/data-warga/{id}/store', 'DataWargaController@storeDataWarga')->name('rw-store-datawarga');
  Route::get('/data-warga/edit/{id}', 'DataWargaController@editDataWarga')->name('rw-edit-datawarga');
  Route::patch('/data-warga/update/{id}', 'DataWargaController@updateDataWarga')->name('rw-update-datawarga');
  Route::get('/data-warga/delete/{id}', 'DataWargaController@deleteDataWarga')->name('rw-delete-datawarga');
});

// is_warga
Route::group(['middleware' => ['is_warga'], 'prefix' => 'warga'], function () {
  Route::get('/', 'WargaController@index')->name('warga');
  Route::get('/pengajuan', 'WargaController@pengajuan')->name('warga-pengajuan');
  Route::get('/pengajuan/surat-pengantar', 'WargaController@pengajuanCreateSuratPengantar')->name('warga-pengajuan-surat-pengantar');
  Route::get('/pengajuan/surat-tidak-mampu', 'WargaController@pengajuanCreateSuratKeteranganTidakMampu')->name('warga-pengajuan-surat-keterangan-tidak-mampu');
  Route::post('/pengajuan/store', 'WargaController@pengajuanStore')->name('warga-pengajuan-store');
});

// is_admin
Route::group(['middleware' => 'is_admin', 'prefix' => 'admin'], function () {
  Route::get('/', 'AdminController@index')->name('admin');
  // KK
  Route::get('/kk', 'DataWargaController@kk')->name('admin-kk');
  Route::get('/kk/baru', 'DataWargaController@formKK')->name('admin-form-kk');
  Route::post('/kk/store', 'DataWargaController@storeKK')->name('admin-store-kk');
  Route::get('/kk/edit/{id}', 'DataWargaController@editKK')->name('admin-edit-kk');
  Route::patch('/kk/update/{id}', 'DataWargaController@updateKK')->name('admin-update-kk');
  Route::get('/kk/delete/{id}', 'DataWargaController@deleteKK')->name('admin-delete-kk');
  Route::get('/kk/show/{id}', 'DataWargaController@showKK')->name('admin-show-kk');
  // Warga
  Route::get('/data-warga', 'DataWargaController@dataWarga')->name('admin-datawarga');
  Route::get('/data-warga/{id}/baru', 'DataWargaController@formDataWarga')->name('admin-form-datawarga');
  Route::post('/data-warga/{id}/store', 'DataWargaController@storeDataWarga')->name('admin-store-datawarga');
  Route::get('/data-warga/edit/{id}', 'DataWargaController@editDataWarga')->name('admin-edit-datawarga');
  Route::patch('/data-warga/update/{id}', 'DataWargaController@updateDataWarga')->name('admin-update-datawarga');
  Route::get('/data-warga/delete/{id}', 'DataWargaController@deleteDataWarga')->name('admin-delete-datawarga');
  // News
  Route::get('/news', 'BeritaController@news')->name('admin-news');
  Route::get('/news/baru', 'BeritaController@newsCreate')->name('admin-news-create');
  Route::post('/news/store', 'BeritaController@newsStore')->name('admin-news-store');
  Route::get('/news/edit/{id}', 'BeritaController@newsEdit')->name('admin-news-edit');
  Route::patch('/news/update/{id}', 'BeritaController@newsUpdate')->name('admin-news-update');
  Route::get('/news/delete/{id}', 'BeritaController@newsDestroy')->name('admin-news-destroy');
});