@if(\Session::has('alert'))
  <div class="alert alert-success" role="alert" style="text-align:right;">
    <p>{{\Session::get('alert')}}</p>
  </div>
@endif

@if(\Session::has('alert-warning'))
  <div class="alert alert-danger" role="alert" style="text-align:right;">
    <p>{{\Session::get('alert-warning')}}</p>
  </div>
@endif
