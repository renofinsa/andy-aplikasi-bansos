<nav class="main-header navbar navbar-expand navbar-white navbar-light">
  <!-- Left navbar links -->
  <ul class="navbar-nav">
    <li class="nav-item">
      <a class="nav-link" data-widget="pushmenu" href="#" role="button"><i class="fas fa-bars"></i></a>
    </li>
  </ul>

  <!-- Right navbar links -->
  <ul class="navbar-nav ml-auto">
    <li class="nav-item dropdown">
      <a class="nav-link btn btn-danger text-white" data-toggle="dropdown" href="#">
        <i class="far fa-user"></i> {{ Auth::user()->nama_lengkap }}
      </a>
      <div class="dropdown-menu dropdown-menu-lg dropdown-menu-right">
        <div class="dropdown-divider"></div>
        <a href="{{ route('user-logout') }}" class="dropdown-item">
          <i class="fas fa-lock mr-2"></i> Keluar
        </a>
        <div class="dropdown-divider"></div>
      </div>
    </li>
  </ul>
</nav>