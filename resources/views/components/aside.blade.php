<aside class="main-sidebar sidebar-dark-primary elevation-4">
  <a href="index3.html" class="brand-link">
    <span class="brand-text font-weight-light">Aplikasi Warga</span>
  </a>
  <div class="sidebar">
    <nav class="mt-2">
      <ul class="nav nav-pills nav-sidebar flex-column mt-5" data-widget="treeview" role="menu" data-accordion="false">
        @if (Auth::user()->id_level_pengguna == 1)
            
        <li class="nav-item">
          <a href="{{ route('admin')}}" class="nav-link {{ (request()->is('admin')) ? 'active' : '' }}">
            <i class="nav-icon fas fa-tachometer-alt"></i>
            <p>
              Beranda
            </p>
          </a>
        </li>
        <li class="nav-item">
          <a href="{{ route('admin-kk')}}" class="nav-link {{ (request()->is('admin/kk')) ? 'active' : '' }}">
            <i class="nav-icon far fa-file"></i>
            <p>
              Kartu Keluarga
            </p>
          </a>
        </li>
        <li class="nav-item">
          <a href="{{ route('admin-datawarga') }}" class="nav-link {{ (request()->is('admin/data-warga')) ? 'active' : '' }}">
            <i class="nav-icon fas fa-users"></i>
            <p>
              Data Warga
            </p>
          </a>
        </li>
        <li class="nav-item">
          <a href="{{ route('admin-news') }}" class="nav-link {{ (request()->is('admin/news')) ? 'active' : '' }}">
            <i class="nav-icon fas fa-newspaper"></i>
            <p>
              Berita
            </p>
          </a>
        </li>
        @endif
        @if (Auth::user()->id_level_pengguna == 4)
        <li class="nav-item">
          <a href="{{ route('warga')}}" class="nav-link {{ (request()->is('warga')) ? 'active' : '' }}">
            <i class="nav-icon fas fa-tachometer-alt"></i>
            <p>
              Beranda
            </p>
          </a>
        </li>
        <li class="nav-item">
          <a href="{{ route('warga-pengajuan') }}" class="nav-link {{ (request()->is('warga/pengajuan')) ? 'active' : '' }}">
            <i class="nav-icon fas fa-envelope-open-text"></i>
            <p>
              Pengajuan
            </p>
          </a>
        </li>
        @endif
        @if (Auth::user()->id_level_pengguna == 2)
        <li class="nav-item">
          <a href="{{ route('rw')}}" class="nav-link {{ (request()->is('rw')) ? 'active' : '' }}">
            <i class="nav-icon fas fa-tachometer-alt"></i>
            <p>
              Beranda
            </p>
          </a>
        </li>
        <li class="nav-item">
          <a href="{{ route('rw-pengajuan') }}" class="nav-link {{ (request()->is('rw/pengajuan')) ? 'active' : '' }}">
            <i class="nav-icon fas fa-envelope-open-text"></i>
            <p>
              Pengajuan
            </p>
          </a>
        </li>
        <li class="nav-item">
          <a href="#" class="nav-link"><i class="fa fa-star"></i><p>Fitur RW</p></a>
        </li>
        <li class="nav-item">
          <a href="{{ route('rw-tanggapi-pengajuan') }}" class="nav-link {{ (request()->is('rw/tanggapi-pengajuan')) ? 'active' : '' }}">
            <i class="nav-icon fas fa-envelope-open-text"></i>
            <p>
              Tanggapi Pengajuan
            </p>
          </a>
        </li>
        <li class="nav-item">
          <a href="{{ route('rw-kk')}}" class="nav-link {{ (request()->is('rw/kk')) ? 'active' : '' }}">
            <i class="nav-icon far fa-file"></i>
            <p>
              Kartu Keluarga
            </p>
          </a>
        </li>
        <li class="nav-item">
          <a href="{{ route('rw-datawarga') }}" class="nav-link {{ (request()->is('rw/data-warga')) ? 'active' : '' }}">
            <i class="nav-icon fas fa-users"></i>
            <p>
              Data Warga
            </p>
          </a>
        </li>
        <li class="nav-item">
          <a href="{{ route('rw-news') }}" class="nav-link {{ (request()->is('rw/news')) ? 'active' : '' }}">
            <i class="nav-icon fas fa-newspaper"></i>
            <p>
              Berita
            </p>
          </a>
        </li>
        @endif
        @if (Auth::user()->id_level_pengguna == 3)
        <li class="nav-item">
          <a href="{{ route('rt')}}" class="nav-link {{ (request()->is('rt')) ? 'active' : '' }}">
            <i class="nav-icon fas fa-tachometer-alt"></i>
            <p>
              Beranda
            </p>
          </a>
        </li>
        <li class="nav-item">
          <a href="{{ route('rt-pengajuan') }}" class="nav-link {{ (request()->is('rt/pengajuan')) ? 'active' : '' }}">
            <i class="nav-icon fas fa-envelope-open-text"></i>
            <p>
              Pengajuan
            </p>
          </a>
        </li>
        <li class="nav-item">
          <a href="#" class="nav-link"><i class="fa fa-star"></i><p>Fitur RT</p></a>
        </li>
        <li class="nav-item">
          <a href="{{ route('rt-tanggapi-pengajuan') }}" class="nav-link {{ (request()->is('rt/tanggapi-pengajuan')) ? 'active' : '' }}">
            <i class="nav-icon fas fa-envelope-open-text"></i>
            <p>
              Tanggapi Pengajuan
            </p>
          </a>
        </li>
        <li class="nav-item">
          <a href="{{ route('rt-kk')}}" class="nav-link {{ (request()->is('rt/kk')) ? 'active' : '' }}">
            <i class="nav-icon far fa-file"></i>
            <p>
              Kartu Keluarga
            </p>
          </a>
        </li>
        <li class="nav-item">
          <a href="{{ route('rt-datawarga') }}" class="nav-link {{ (request()->is('rt/data-warga')) ? 'active' : '' }}">
            <i class="nav-icon fas fa-users"></i>
            <p>
              Data Warga
            </p>
          </a>
        </li>
        <li class="nav-item">
          <a href="{{ route('rt-news') }}" class="nav-link {{ (request()->is('rt/news')) ? 'active' : '' }}">
            <i class="nav-icon fas fa-newspaper"></i>
            <p>
              Berita
            </p>
          </a>
        </li>
        @endif
      </ul>
    </nav>
    <!-- /.sidebar-menu -->
  </div>
  <!-- /.sidebar -->
</aside>