<html>
<head>
 <title>Agenda Surat Keluar</title>
 <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
</head>
<body>
 
 <center>
 <h5>Agenda Surat Keluar Tahun 2020 - 2021</h4>
 </center>
 
 <table class='table table-bordered'>
 <thead>
  <tr>
    <th>No.</th>
    <th>Tanggal</th>
    <th>Nomor Surat</th>
    <th>Perihal</th>
    <th>Nomor KTP</th>
    <th>Nama</th>
    <th>Status</th>
  </tr>
 </thead>
 <tbody>
 @php $i=1 @endphp
 @foreach($data as $item)
 <tr>
 <td>{{ $i++ }}</td>
 <td>{{ date('d M Y', strtotime($item->tanggal_pengajuan)) }}</td>
 <td>{{ $item->id}}{{ date('dmY', strtotime($item->tanggal_pengajuan)) }}</td>
 <td>{{ $item->jenis->nama }}</td>
 <td>{{ $item->user->id }}</td>
 <td>{{ $item->user->nama_lengkap }}</td>
 <td style="text-transform: uppercase">{{ $item->status }}</td>
 </tr>
 @endforeach
 </tbody>
 </table>
 
</body>
</html>