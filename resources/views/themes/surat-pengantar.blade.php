<html>
<head>
 <title>Surat Pengantar</title>
 <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">

  <style>
    h5 {
      text-transform: uppercase
    }
  </style>
</head>
<body>
 
 <center>
 <h5>Kota {{$user->kk($user->id_kartu_keluarga)['kota']}}</h5>
 <h5>Kecamatan {{$user->kk($user->id_kartu_keluarga)['kecamatan']}}</h5>
 <h5>Kelurahan {{$user->kk($user->id_kartu_keluarga)['kelurahan']}}</h5>
 <h5>RT.{{$user->kk($user->id_kartu_keluarga)['rt']}} / RW.{{$user->kk($user->id_kartu_keluarga)['rw']}}</h5>
<hr style="border: 5px solid black">
<h5>Surat Pengantar</h5>
  <h5 style="font-size: 18px">NO: {{ $data->id}}{{ date('dmY', strtotime($data->tanggal_pengajuan)) }}</h5>
 </center>
 
 <p>Yang bertanda tangan dibawah ini, Ketua RT.{{$user->kk($user->id_kartu_keluarga)['rt']}}/{{$user->kk($user->id_kartu_keluarga)['rw']}}, Kelurahan {{$user->kk($user->id_kartu_keluarga)['kelurahan']}}, Kecamatan {{$user->kk($user->id_kartu_keluarga)['kecamatan']}}. Dengan ini menerangkan bahwa:</p>
 
 <table class='table'>
   <tbody>
    <tr>
      <th width="200">Nama</th>
      <td>: {{ $user->nama_lengkap }}</td>
    </tr>
    <tr>
      <th width="200">Tempat/Tgl Lahir</th>
      <td>: {{ $user->tempat_lahir }} {{ date('d M Y', strtotime($data->tanggal_lahir)) }}</td>
    </tr>
    <tr>
      <th width="200">No NIK</th>
      <td>: {{ $user->id }}</td>
    </tr>
    <tr>
      <th width="200">Alamat</th>
      <td>: {{$user->kk($user->id_kartu_keluarga)['alamat']}}</td>
    </tr>
    <tr>
      <th width="200">Agama</th>
      <td>: {{ $user->agama }}</td>
    </tr>
    <tr>
      <th width="200">Status Perkawinan</th>
      <td>: {{ $user->status_perkawinan }}</td>
    </tr>
    <tr>
      <th width="200">Kewarganegaraan</th>
      <td>: {{ $user->wni == 1 ? 'WNI' : 'WNA' }}</td>
    </tr>
    <tr>
      <th width="200">Pekerjaan</th>
      <td>: {{ $user->pekerjaan }}</td>
    </tr>
    <tr>
      <th width="200">Keperluan</th>
      <td>: {{ $data->jenis->nama }}</td>
    </tr>
   </tbody>
 </table>

  <section style="margin-top: 5em">
    <div style="float: left">
      <p>Nomor: {{ $data->id}}{{ date('dmY', strtotime($data->tanggal_pengajuan)) }} <br>Ketua RT</p>
      <img style="width: 100px" src="img/ttd.png" alt="">
      <p>{{$rt->nama_lengkap}}</p>
    </div>
    <div style="float: right">
      <p>Jakarta, {{date('d, M Y')}} <br> Ketua RW</p>
      <img style="width: 100px" src="img/ttd.png" alt="">
      <p>{{$rw->nama_lengkap}}</p>
    </div>
  </section>
</body>
</html>