@extends('layouts.master')

@section('title', 'Pengajuan | Aplikasi Bansos Warga')

@section('headline', $head)

@section('breadcrumb')
  <li class="breadcrumb-item"><a href="#">Pengajuan</a></li>
  <li class="breadcrumb-item active">Baru</li>
@endsection

@section('content')
<div class="row justify-content-center">
  <div class="col-md-6 col-sm-12">
    <div class="card">
      <div class="card-header bg-dark">
      </div>
      <!-- /.card-header -->
        @if (Auth::user()->id_level_pengguna == 4)
        <form action="{{ route('warga-pengajuan-store') }}" method="post">
        @endif
        @if (Auth::user()->id_level_pengguna == 2)
        <form action="{{ route('rw-pengajuan-store') }}" method="post">
        @endif
        @if (Auth::user()->id_level_pengguna == 3)
        <form action="{{ route('rt-pengajuan-store') }}" method="post">
        @endif
        @csrf
        <div class="card-body">
          <div class="form-group">
            <label for="id_jenis_pengajuan">Jenis Pengajuan <span class="text-danger">*</span></label>
            <select name="id_jenis_pengajuan" class="form-control @error('id_jenis_pengajuan') is-invalid @enderror">
              <option value="">Pilih Jenis Pengajuan</option>
              @foreach ($data as $key=>$item)
                <option value="{{ $item->id }}">{{ $item->nama }}</option>
              @endforeach
            </select>
            @error('id_jenis_pengajuan')
              <span class="invalid-feedback" role="alert">
                <strong>{{ $message }}</strong>
              </span>
            @enderror
          </div>
          <div class="form-group">
            <label for="pesan">Pesan <span class="text-danger">*</span></label>
            <textarea name="pesan" cols="30" rows="10" class="form-control @error('pesan') is-invalid @enderror" placeholder="Kpd Yth pengurus RT, mohon bantu dibuatkan surat ...">{{ old('pesan') }}</textarea>
            @error('pesan')
              <span class="invalid-feedback" role="alert">
                <strong>{{ $message }}</strong>
              </span>
            @enderror
          </div>
          <div class="form-group">
            <button type="submit" class="btn btn-primary form-control btn-sm">Kirim Pengajuan</button>
          </div>
        </div>
      </form>
      <!-- /.card-body -->
    </div>
  </div>
</div>
@endsection