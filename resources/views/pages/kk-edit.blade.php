@extends('layouts.master')

@section('title', 'Ubah Kartu Keluarga | Aplikasi Bansos Warga')

@section('headline', 'Ubah Kartu Keluarga')

@section('breadcrumb')
  <li class="breadcrumb-item"><a href="#">Kartu Keluarga</a></li>
  <li class="breadcrumb-item active">Ubah</li>
@endsection

@section('content')
<div class="row justify-content-center">
  <div class="col-md-6 col-sm-12">
    <div class="card">
      <div class="card-header bg-dark">
      </div>
      <!-- /.card-header -->
       <!-- /.card-header -->
      @if (Auth::user()->id_level_pengguna == 1)
        <form action="{{ route('admin-update-kk', $data->id) }}" method="post">
      @endif
      @if (Auth::user()->id_level_pengguna == 2)
        <form action="{{ route('rw-update-kk', $data->id) }}" method="post">
      @endif
      @if (Auth::user()->id_level_pengguna == 3)
        <form action="{{ route('rt-update-kk', $data->id) }}" method="post">
      @endif
        @csrf
        <input type="hidden" name="_method" value="patch">
        <div class="card-body">
          <div class="form-group">
            <label for="id">Nomor Kartu Keluarga (KK) <span class="text-danger">*</span></label>
            <input type="text" class="form-control" name="id" value="{{ $data->id }}"  disabled>
          </div>
          <div class="row">
            <div class="col-md-6 col-sm-12">
              <div class="form-group">
                <label for="rt">RT <span class="text-danger">*</span></label>
                <input type="text" class="form-control @error('rt') is-invalid @enderror" name="rt" value="{{ $data->rt }}" placeholder="011">
                @error('rt')
                  <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                  </span>
                @enderror
              </div>
            </div>
            <div class="col-md-6 col-sm-12">
              <div class="form-group">
                <label for="rw">RW <span class="text-danger">*</span></label>
                <input type="text" class="form-control @error('rw') is-invalid @enderror" name="rw" value="{{ $data->rw }}" placeholder="009">
                @error('rw')
                  <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                  </span>
                @enderror
              </div>
            </div>
            <div class="col-md-6 col-sm-12">
              <div class="form-group">
                <label for="kelurahan">Kelurahan <span class="text-danger">*</span></label>
                <input type="text" class="form-control @error('kelurahan') is-invalid @enderror" name="kelurahan" value="{{ $data->kelurahan }}" placeholder="Rawa Buaya">
                @error('kelurahan')
                  <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                  </span>
                @enderror
              </div>
            </div>
            <div class="col-md-6 col-sm-12">
              <div class="form-group">
                <label for="kecamatan">Kecamatan <span class="text-danger">*</span></label>
                <input type="text" class="form-control @error('kecamatan') is-invalid @enderror" name="kecamatan" value="{{ $data->kecamatan }}" placeholder="Cengkareng">
                @error('kecamatan')
                  <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                  </span>
                @enderror
              </div>
            </div>
            <div class="col-md-6 col-sm-12">
              <div class="form-group">
                <label for="kota">Kota <span class="text-danger">*</span></label>
                <input type="text" class="form-control @error('kota') is-invalid @enderror" name="kota" value="{{ $data->kota }}" placeholder="Jakarta Barat">
                @error('kota')
                  <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                  </span>
                @enderror
              </div>
            </div>
            <div class="col-md-6 col-sm-12">
              <div class="form-group">
                <label for="provinsi">Provinsi <span class="text-danger">*</span></label>
                <input type="text" class="form-control @error('provinsi') is-invalid @enderror" name="provinsi" value="{{ $data->provinsi }}" placeholder="DKI Jakarta">
                @error('provinsi')
                  <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                  </span>
                @enderror
              </div>
            </div>
          </div>
          <div class="form-group">
            <label for="alamat">Alamat <span class="text-danger">*</span></label>
            <textarea name="alamat" cols="30" rows="5" class="form-control @error('alamat') is-invalid @enderror" placeholder="Jl permai raya no 19">{{ $data->alamat }}</textarea>
            @error('alamat')
              <span class="invalid-feedback" role="alert">
                <strong>{{ $message }}</strong>
              </span>
            @enderror
          </div>
          <div class="form-group">
            <label for="kode_pos">Kode Pos <span class="text-danger">*</span></label>
            <input type="text" class="form-control @error('kode_pos') is-invalid @enderror" name="kode_pos" value="{{ $data->kode_pos }}" placeholder="14045">
            @error('kode_pos')
              <span class="invalid-feedback" role="alert">
                <strong>{{ $message }}</strong>
              </span>
            @enderror
          </div>
          <div class="form-group">
            <button type="submit" class="btn btn-primary form-control btn-sm">Simpan Dan Ubah Kartu Keluarga</button>
          </div>
        </div>
      </form>
      <!-- /.card-body -->
    </div>
  </div>
</div>
@endsection