@extends('layouts.master')

@section('title', 'Anggota Keluarga | Aplikasi Bansos Warga')

@section('headline', 'Anggota Keluarga')

@section('breadcrumb')
  <li class="breadcrumb-item"><a href="#">Kartu Keluarga</a></li>
  <li class="breadcrumb-item"><a href="#">Data Anggota Keluarga</a></li>
  <li class="breadcrumb-item active">Tambah</li>
@endsection

@section('content')
<div class="row justify-content-center">
  <div class="col-md-6 col-sm-12">
    <div class="card">
      <div class="card-header bg-dark">
      </div>
      <!-- /.card-header -->
      @if (Auth::user()->id_level_pengguna == 1)
      <form action="{{ route('admin-store-datawarga', $data->id) }}" method="post">
        @endif
      @if (Auth::user()->id_level_pengguna == 2)
        <form action="{{ route('rw-store-datawarga', $data->id) }}" method="post">
      @endif
      @if (Auth::user()->id_level_pengguna == 3)
        <form action="{{ route('rt-store-datawarga', $data->id) }}" method="post">
      @endif
  
        @csrf
        <div class="card-body">
          <div class="form-group">
            <label for="id">Nomor Kartu Tanda Penduduk (KTP) <span class="text-danger">*</span></label>
            <input type="text" class="form-control @error('id') is-invalid @enderror" name="id" value="{{ old('id') }}" placeholder="Masukan Nomor KTP" >
            @error('id')
              <span class="invalid-feedback" role="alert">
                <strong>{{ $message }}</strong>
              </span>
            @enderror
          </div>
          <div class="form-group">
            <label for="nama_lengkap">Nama Lengkap <span class="text-danger">*</span></label>
            <input type="text" class="form-control @error('nama_lengkap') is-invalid @enderror" name="nama_lengkap" value="{{ old('nama_lengkap') }}" placeholder="Masukan Nama Lengap" >
            @error('nama_lengkap')
              <span class="invalid-feedback" role="alert">
                <strong>{{ $message }}</strong>
              </span>
            @enderror
          </div>
          <div class="form-group">
            <label for="jenis_kelamin">Jenis Kelamin <span class="text-danger">*</span></label>
            <div class="row pl-2">
              <div class="col-6 form-check">
                <input class="form-check-input" type="radio" name="jenis_kelamin" id="pria" value="1" checked>
                <label class="form-check-label" for="pria">
                  Pria
                </label>
              </div>
              <div class="col-6 form-check">
                <input class="form-check-input" type="radio" name="jenis_kelamin" id="wanita" value="0">
                <label class="form-check-label" for="wanita">
                  Wanita
                </label>
              </div>
            </div>
            @error('jenis_kelamin')
              <span class="invalid-feedback" role="alert">
                <strong>{{ $message }}</strong>
              </span>
            @enderror
          </div>
          <div class="row">
            <div class="col-md-6 col-sm-12">
              <div class="form-group">
                <label for="tempat_lahir">Tempat Lahir <span class="text-danger">*</span></label>
                <input type="text" class="form-control @error('tempat_lahir') is-invalid @enderror" name="tempat_lahir" value="{{ old('tempat_lahir') }}" placeholder="Jakarta">
                @error('tempat_lahir')
                  <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                  </span>
                @enderror
              </div>
            </div>
            <div class="col-md-6 col-sm-12">
              <div class="form-group">
                <label for="tanggal_lahir">Tanggal Lahir <span class="text-danger">*</span></label>
                <input type="date" class="form-control @error('tanggal_lahir') is-invalid @enderror" name="tanggal_lahir" value="{{ old('tanggal_lahir') }}">
                @error('tanggal_lahir')
                  <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                  </span>
                @enderror
              </div>
            </div>
            <div class="col-md-6 col-sm-12">
              <div class="form-group">
                <label for="agama">Agama <span class="text-danger">*</span></label>
                <select name="agama" class="form-control @error('agama') is-invalid @enderror">
                  <option value="">Pilih Agama</option>
                  <option value="Islam">Islam</option>
                  <option value="Katolik">Katolik</option>
                  <option value="Kristen">Kristen</option>
                  <option value="Buddha">Buddha</option>
                  <option value="Hindu">Hindu</option>
                  <option value="Khonghucu">Khonghucu</option>
                </select>
                @error('agama')
                  <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                  </span>
                @enderror
              </div>
            </div>
            <div class="col-md-6 col-sm-12">
              <div class="form-group">
                <label for="agama">Pendidikan Terakhir <span class="text-danger">*</span></label>
                <select name="pendidikan_terakhir" class="form-control @error('pendidikan_terakhir') is-invalid @enderror">
                  <option value="">Pilih Pendidikan Terakhir</option>
                  <option value="SD">SD</option>
                  <option value="SMP">SMP</option>
                  <option value="SMA/SMK">SMA/SMK</option>
                  <option value="Belum Sekolah">Belum Sekolah</option>
                  <option value="-">-</option>
                </select>
                @error('pendidikan_terakhir')
                  <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                  </span>
                @enderror
              </div>
            </div>
            <div class="col-md-6 col-sm-12">
              <div class="form-group">
                <label for="agama">Pekerjaan <span class="text-danger">*</span></label>
                <select name="pekerjaan" class="form-control @error('pekerjaan') is-invalid @enderror">
                  <option value="">Pilih Pekerjaan</option>
                  <option value="Buruh">Buruh</option>
                  <option value="Pegawai Negeri">Pegawai Negeri</option>
                  <option value="Pegawai Swasta">Pegawai Swasta</option>
                  <option value="Pelajar">Pelajar</option>
                  <option value="Ibu Rumah Tangga">Ibu Rumah Tangga</option>
                </select>
                @error('pekerjaan')
                  <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                  </span>
                @enderror
              </div>
            </div>
            <div class="col-md-6 col-sm-12">
              <div class="form-group">
                <label for="golongan_darah">Golongan Darah <span class="text-danger">*</span></label>
                <select name="golongan_darah" class="form-control @error('golongan_darah') is-invalid @enderror">
                  <option value="">Pilih Golongan Darah</option>
                  <option value="A">A</option>
                  <option value="O">O</option>
                  <option value="B">B</option>
                  <option value="AB">AB</option>
                  <option value="-">-</option>
                </select>
                @error('golongan_darah')
                  <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                  </span>
                @enderror
              </div>
            </div>
            <div class="col-md-6 col-sm-12">
              <div class="form-group">
                <label for="status_perkawinan">Status Perkawinan <span class="text-danger">*</span></label>
                <select name="status_perkawinan" class="form-control @error('status_perkawinan') is-invalid @enderror">
                  <option value="">Pilih Status Perkawinan</option>
                  <option value="Menikah">Menikah</option>
                  <option value="Kawin Tidak Tercatat">Kawin Tidak Tercatat</option>
                  <option value="Belum Menikah">Belum Menikah</option>
                  <option value="Duda">Duda</option>
                  <option value="Janda">Janda</option>
                  <option value="-">-</option>
                </select>
                @error('status_perkawinan')
                  <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                  </span>
                @enderror
              </div>
            </div>
            <div class="col-md-6 col-sm-12">
              <div class="form-group">
                <label for="status_keluarga">Status Dalam Keluarga <span class="text-danger">*</span></label>
                <select name="status_keluarga" class="form-control @error('status_keluarga') is-invalid @enderror">
                  <option value="">Pilih Status</option>
                  <option value="Kepala Keluarga">Kepala Keluarga</option>
                  <option value="Istri">Istri</option>
                  <option value="Anak">Anak</option>
                  <option value="Orang Tua">Orang Tua</option>
                  <option value="DLL">DLL</option>
                </select>
                @error('status_keluarga')
                  <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                  </span>
                @enderror
              </div>
            </div>
          </div>
          <div class="form-group">
            <label for="wni">Status Kewarganegaraan <span class="text-danger">*</span></label>
            <div class="row pl-2">
              <div class="col-6 form-check">
                <input class="form-check-input" type="radio" name="wni" id="wni" value="1" checked>
                <label class="form-check-label" for="wni">
                  WNI
                </label>
              </div>
              <div class="col-6 form-check">
                <input class="form-check-input" type="radio" name="wni" id="wna" value="0">
                <label class="form-check-label" for="wna">
                  WNA
                </label>
              </div>
            </div>
            @error('wni')
              <span class="invalid-feedback" role="alert">
                <strong>{{ $message }}</strong>
              </span>
            @enderror
          </div>
          <div class="form-group">
            <label for="nama_ayah">Nama Ayah <span class="text-danger">*</span></label>
            <input type="text" class="form-control @error('nama_ayah') is-invalid @enderror" name="nama_ayah" value="{{ old('nama_ayah') }}" placeholder="Masukan Nama Ayah" >
            @error('nama_ayah')
              <span class="invalid-feedback" role="alert">
                <strong>{{ $message }}</strong>
              </span>
            @enderror
          </div>
          <div class="form-group">
            <label for="nama_ibu">Nama Ibu <span class="text-danger">*</span></label>
            <input type="text" class="form-control @error('nama_ibu') is-invalid @enderror" name="nama_ibu" value="{{ old('nama_ibu') }}" placeholder="Masukan Nama Ibu" >
            @error('nama_ibu')
              <span class="invalid-feedback" role="alert">
                <strong>{{ $message }}</strong>
              </span>
            @enderror
          </div>
          <div class="form-group">
            <label for="id_level_pengguna">Level Pengguna <span class="text-danger">*</span></label>
            <select name="id_level_pengguna" class="form-control @error('id_level_pengguna') is-invalid @enderror">
              <option value="">Pilih Level Pengguna</option>
              <option value="4">Warga</option>
              <option value="2">RW</option>
              <option value="3">RT</option>
            </select>
            @error('id_level_pengguna')
              <span class="invalid-feedback" role="alert">
                <strong>{{ $message }}</strong>
              </span>
            @enderror
          </div>
          <div class="form-group">
            <button type="submit" class="btn btn-primary form-control btn-sm">Tambah Anggota</button>
          </div>
        </div>
      </form>
      <!-- /.card-body -->
    </div>
  </div>
</div>
@endsection