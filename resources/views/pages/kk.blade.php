@extends('layouts.master')

@section('title', 'Kartu Keluarga | Aplikasi Bansos Warga')

@section('css')
  <!-- DataTables -->
  <link rel="stylesheet" href="{{ asset('plugins/datatables-bs4/css/dataTables.bootstrap4.min.css') }}">
  <link rel="stylesheet" href="{{ asset('plugins/datatables-responsive/css/responsive.bootstrap4.min.css') }}">
  <link rel="stylesheet" href="{{ asset('plugins/datatables-buttons/css/buttons.bootstrap4.min.css') }}">
@endsection

@section('headline', 'Kartu Keluarga')

@section('breadcrumb')
  <li class="breadcrumb-item active">Kartu Keluarga</li>
@endsection

@section('content')
<div class="card">
  <div class="card-header bg-dark">
  </div>
  <!-- /.card-header -->
  <div class="card-body">
    @if (Auth::user()->id_level_pengguna == 1)
      <a href="{{ route('admin-form-kk') }}" class="btn btn-success btn-sm float-left"><i class="fa fa-plus"></i> Tambah Kartu Keluarga</a>
    @endif
    @if (Auth::user()->id_level_pengguna == 2)
      <a href="{{ route('rw-form-kk') }}" class="btn btn-success btn-sm float-left"><i class="fa fa-plus"></i> Tambah Kartu Keluarga</a>
    @endif
    @if (Auth::user()->id_level_pengguna == 3)
      <a href="{{ route('rt-form-kk') }}" class="btn btn-success btn-sm float-left"><i class="fa fa-plus"></i> Tambah Kartu Keluarga</a>
    @endif
    <table id="example1" class="table table-bordered table-striped">
      <thead>
      <tr>
        <th>No.</th>
        <th>Nomor KK</th>
        <th>Kepala Keluarga</th>
        <th>Total Anggota Keluarga</th>
        <th width="60">Aksi</th>
      </tr>
      </thead>
      <tbody>
        @foreach ($data as $key=>$item)
        <tr>
          <td>{{ $key+1 }}</td>
          <td>{{ $item->id }}</td>
          <td>{{ !$item->kepala($item->id)['nama_lengkap'] ? '-' : $item->kepala($item->id)['nama_lengkap'] }}</td>
          <td> 
            @if (Auth::user()->id_level_pengguna == 1)
            <a href="{{ route('admin-show-kk', $item->id) }}" class="btn btn-dark btn-sm color-white">{{ count($item->anggota) }} </a>
            @endif
            @if (Auth::user()->id_level_pengguna == 2)
            <a href="{{ route('rw-show-kk', $item->id) }}" class="btn btn-dark btn-sm color-white">{{ count($item->anggota) }} </a>
            @endif
            @if (Auth::user()->id_level_pengguna == 3)
            <a href="{{ route('rt-show-kk', $item->id) }}" class="btn btn-dark btn-sm color-white">{{ count($item->anggota) }} </a>
            @endif
        
          </td>
          <td>
            @if (Auth::user()->id_level_pengguna == 1)
              <a href="{{ route('admin-edit-kk', $item->id) }}" class="btn btn-primary btn-sm float-left"><i class="fa fa-pencil-alt"></i></a>
              <a href="{{ route('admin-delete-kk', $item->id) }}" class="btn btn-danger btn-sm float-right"><i class="fa fa-trash"></i></a>
            @endif
            @if (Auth::user()->id_level_pengguna == 2)
              <a href="{{ route('rw-edit-kk', $item->id) }}" class="btn btn-primary btn-sm float-left"><i class="fa fa-pencil-alt"></i></a>
              <a href="{{ route('rw-delete-kk', $item->id) }}" class="btn btn-danger btn-sm float-right"><i class="fa fa-trash"></i></a>
            @endif
            @if (Auth::user()->id_level_pengguna == 3)
              <a href="{{ route('rt-edit-kk', $item->id) }}" class="btn btn-primary btn-sm float-left"><i class="fa fa-pencil-alt"></i></a>
              <a href="{{ route('rt-delete-kk', $item->id) }}" class="btn btn-danger btn-sm float-right"><i class="fa fa-trash"></i></a>
            @endif
          </td>
        </tr>
        @endforeach
      </tfoot>
    </table>
  </div>
  <!-- /.card-body -->
</div>
@endsection

@section('js')
<!-- DataTables  & Plugins -->
<script src="{{ asset('plugins/datatables/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('plugins/datatables-bs4/js/dataTables.bootstrap4.min.js') }}"></script>
<script src="{{ asset('plugins/datatables-responsive/js/dataTables.responsive.min.js') }}"></script>
<script src="{{ asset('plugins/datatables-responsive/js/responsive.bootstrap4.min.js') }}"></script>
<script src="{{ asset('plugins/datatables-buttons/js/dataTables.buttons.min.js') }}"></script>
<script src="{{ asset('plugins/datatables-buttons/js/buttons.bootstrap4.min.js') }}"></script>
<script src="{{ asset('plugins/jszip/jszip.min.js') }}"></script>
<script src="{{ asset('plugins/pdfmake/pdfmake.min.js') }}"></script>
<script src="{{ asset('plugins/pdfmake/vfs_fonts.js') }}"></script>
<script src="{{ asset('plugins/datatables-buttons/js/buttons.html5.min.js') }}"></script>
<script src="{{ asset('plugins/datatables-buttons/js/buttons.print.min.js') }}"></script>
<script src="{{ asset('plugins/datatables-buttons/js/buttons.colVis.min.js') }}"></script>

<script>
  $(function () {
    $("#example1").DataTable({
      "responsive": true, "lengthChange": false, "autoWidth": false,
      // "buttons": ["copy", "csv", "excel", "pdf", "print", "colvis"]
    }).buttons().container().appendTo('#example1_wrapper .col-md-6:eq(0)');
  });
</script>
@endsection