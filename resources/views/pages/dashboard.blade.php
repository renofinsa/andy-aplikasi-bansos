@extends('layouts.master')

@section('title', 'Dashboard | Aplikasi Bansos Warga')

@section('headline', 'Dashboard')

@section('breadcrumb')
@endsection

@section('content')
<div class="row justify-content-center">
  <h4>Berita Warga</h4>
</div>
@foreach ($data as $item)
<div class="row justify-content-center">
  <div class="col-md-6 col-sm-12">
    <div class="card">
      <div class="card-header bg-primary">{{ $item->judul }} </div>
      <div class="card-body">
        {!! $item->deskripsi !!}
      </div>
      <div class="card-footer">
        <span class="float-right"><i class="fa fa-user"></i> {{ $item->author->nama_lengkap}}</span>
        <span class="float-left"><i class="fa fa-calendar"></i> {{ date('d M Y', strtotime($item->tanggal_buat)) }}</span>
      </div>
    </div>
  </div>
</div>
@endforeach
@endsection