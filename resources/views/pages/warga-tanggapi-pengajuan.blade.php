@extends('layouts.master')

@section('title', 'Pengajuan Warga | Aplikasi Bansos Warga')

@section('css')
  <!-- DataTables -->
  <link rel="stylesheet" href="{{ asset('plugins/datatables-bs4/css/dataTables.bootstrap4.min.css') }}">
  <link rel="stylesheet" href="{{ asset('plugins/datatables-responsive/css/responsive.bootstrap4.min.css') }}">
  <link rel="stylesheet" href="{{ asset('plugins/datatables-buttons/css/buttons.bootstrap4.min.css') }}">
@endsection

@section('headline', 'Pengajuan Warga')

@section('breadcrumb')
  <li class="breadcrumb-item active">Pengajuan Warga</li>
@endsection

@section('content')
<div class="card">
  <div class="card-header bg-dark">
  </div>
  <!-- /.card-header -->
  <div class="card-body">
    <a class="btn btn-success" href="{{ route('export-data-pengajuan') }}" target="_blank"> <i class="fa fa-download"></i> Download</a>
    <table id="example1" class="table table-bordered table-striped">
      <thead>
      <tr>
        <th>No.</th>
        <th>Jenis Pengajuan</th>
        <th>Tanggal Buat</th>
        <th>Status</th>
        @if (Auth::user()->id_level_pengguna !== 4)
        <th>Pengirim</th>
        <th>Aksi</th>
        @endif
      </tr>
      </thead>
      <tbody>
        @foreach ($data as $key=>$item)
        <tr>
          <td>{{ $key+1 }}</td>
          <td>{{ $item->jenis->nama }}</td>
          <td>{{ date('d M Y', strtotime($item->tanggal_pengajuan)) }}</td>
          <td>{{ $item->status }}</td>
          @if (Auth::user()->id_level_pengguna !== 4)
          <td>{{ $item->user->nama_lengkap }}</td>
          <td>
            <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModal{{ $item->id }}">
              <i class="fa fa-eye"></i> Lihat
            </button>
          

            <!-- Modal -->
            <div class="modal fade" id="exampleModal{{ $item->id }}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
              <div class="modal-dialog" role="document">
                <div class="modal-content">
                  <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Pengajuan</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                      <span aria-hidden="true">&times;</span>
                    </button>
                  </div>
                  <div class="modal-body">
                    <p><strong>Pengirim:</strong> {{ $item->user->nama_lengkap }}</p>
                    <p><strong>Diajukan Tanggal:</strong> {{ date('d M Y', strtotime($item->tanggal_pengajuan)) }}</p>
                    <p><strong>Tujuan Pengajuan:</strong> {{ $item->jenis->nama }}</p>
                    <p><strong>Pesan:</strong> {{ $item->pesan }}</p>
                    <hr>
                    @if (Auth::user()->id_level_pengguna == 1)
                      <form action="{{ route('admin-pengajuan-action') }}" method="post">
                    @endif
                    @if (Auth::user()->id_level_pengguna == 2)
                      <form action="{{ route('rw-pengajuan-action') }}" method="post">

                    @endif
                    @if (Auth::user()->id_level_pengguna == 3)
                      <form action="{{ route('rt-pengajuan-action') }}" method="post">
                    @endif
                      @csrf
                      @if ($item->status == 'diproses rt' && Auth::user()->id_level_pengguna == 3)
                      <input type="hidden" name="id" value="{{ $item->id }}">
                      <div class="form-check">
                        <input name="status" class="form-check-input" type="checkbox" value="diproses rw" id="defaultCheck{{ $item->id }}" required>
                        <label class="form-check-label" for="defaultCheck{{ $item->id }}">
                          Disetujui dan dilanjutkan ke RW
                        </label>
                      </div>
                      @endif
                      @if ($item->status == 'diproses rw' && Auth::user()->id_level_pengguna == 2)
                      <input type="hidden" name="id" value="{{ $item->id }}">
                      <div class="form-check">
                        <input name="status" class="form-check-input" type="checkbox" value="diterima" id="defaultCheck{{ $item->id }}" required>
                        <label class="form-check-label" for="defaultCheck{{ $item->id }}">
                          Disetujui dan dikirim kepada warga
                        </label>
                      </div>
                      @endif
                  </div>
                  <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    @if ($item->status == 'diproses rt' && Auth::user()->id_level_pengguna == 3)
                    <button type="submit" class="btn btn-primary">Kirim Tanggapan</button>
                    @endif
                    @if ($item->status == 'diproses rw' && Auth::user()->id_level_pengguna == 2)
                    <button type="submit" class="btn btn-primary">Kirim Tanggapan</button>
                    @endif
                  </div>
                </form>
                </div>
              </div>
            </div>
          </td>
          @endif
        </tr>
        @endforeach
      </tfoot>
    </table>
  </div>
  <!-- /.card-body -->
</div>
@endsection

@section('js')
<!-- DataTables  & Plugins -->
<script src="{{ asset('plugins/datatables/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('plugins/datatables-bs4/js/dataTables.bootstrap4.min.js') }}"></script>
<script src="{{ asset('plugins/datatables-responsive/js/dataTables.responsive.min.js') }}"></script>
<script src="{{ asset('plugins/datatables-responsive/js/responsive.bootstrap4.min.js') }}"></script>
<script src="{{ asset('plugins/datatables-buttons/js/dataTables.buttons.min.js') }}"></script>
<script src="{{ asset('plugins/datatables-buttons/js/buttons.bootstrap4.min.js') }}"></script>
<script src="{{ asset('plugins/jszip/jszip.min.js') }}"></script>
<script src="{{ asset('plugins/pdfmake/pdfmake.min.js') }}"></script>
<script src="{{ asset('plugins/pdfmake/vfs_fonts.js') }}"></script>
<script src="{{ asset('plugins/datatables-buttons/js/buttons.html5.min.js') }}"></script>
<script src="{{ asset('plugins/datatables-buttons/js/buttons.print.min.js') }}"></script>
<script src="{{ asset('plugins/datatables-buttons/js/buttons.colVis.min.js') }}"></script>

<script>
  $(function () {
    $("#example1").DataTable({
      "responsive": true, "lengthChange": false, "autoWidth": false,
      // "buttons": ["copy", "csv", "excel", "pdf", "print", "colvis"]
    }).buttons().container().appendTo('#example1_wrapper .col-md-6:eq(0)');
  });
</script>
@endsection