@extends('layouts.master')

@section('title', 'Data Anggota Keluarga | Aplikasi Bansos Warga')

@section('css')
  <!-- DataTables -->
  <link rel="stylesheet" href="{{ asset('plugins/datatables-bs4/css/dataTables.bootstrap4.min.css') }}">
  <link rel="stylesheet" href="{{ asset('plugins/datatables-responsive/css/responsive.bootstrap4.min.css') }}">
  <link rel="stylesheet" href="{{ asset('plugins/datatables-buttons/css/buttons.bootstrap4.min.css') }}">
@endsection

@section('headline', 'Data Anggota Keluarga')

@section('breadcrumb')
  <li class="breadcrumb-item active"><a href="{{ route('admin-kk') }}">Kartu Keluarga</a></li>
  <li class="breadcrumb-item active">Data Anggota Keluarga</li>
@endsection

@section('content')
<div class="card">
  <div class="card-header bg-dark">
  </div>
  <!-- /.card-header -->
  <div class="card-body">
    @if (Auth::user()->id_level_pengguna == 1)
     <a href="{{ route('admin-form-datawarga', $id) }}" class="btn btn-success btn-sm float-left"><i class="fa fa-plus"></i> Tambah Anggota Keluarga</a>
    @endif
    @if (Auth::user()->id_level_pengguna == 2)
     <a href="{{ route('rw-form-datawarga', $id) }}" class="btn btn-success btn-sm float-left"><i class="fa fa-plus"></i> Tambah Anggota Keluarga</a>
    @endif
    @if (Auth::user()->id_level_pengguna == 3)
     <a href="{{ route('rt-form-datawarga', $id) }}" class="btn btn-success btn-sm float-left"><i class="fa fa-plus"></i> Tambah Anggota Keluarga</a>
    @endif

    <table id="example1" class="table table-bordered table-striped">
      <thead>
      <tr>
        <th>No.</th>
        <th>Nomor KTP</th>
        <th>Nama Lengkap</th>
        <th>Jenis Kelamin</th>
        <th>Tempat, Tanggal Lahir</th>
        <th>Status</th>
        <th width="60">Aksi</th>
      </tr>
      </thead>
      <tbody>
        @foreach ($data as $key=>$item)
        <tr>
          <td>{{ $key+1 }}</td>
          <td>{{ $item->id }}</td>
          <td>{{ $item->nama_lengkap }}</td>
          <td>{{ $item->jenis_kelamin == 1 ? 'Pria' : 'Wanita'}}</td>
          <td>{{ $item->tempat_lahir }}, {{ $item->tanggal_lahir }}</td>
          <td>{{ $item->status_keluarga }}</td>
          <td>
              @if (Auth::user()->id_level_pengguna == 1)
                <a href="{{ route('admin-edit-datawarga', $item->id) }}" class="btn btn-primary btn-sm float-left"><i class="fa fa-pencil-alt"></i></a>
                <a href="" class="btn btn-danger btn-sm float-right"><i class="fa fa-trash"></i></a>
              @endif
              @if (Auth::user()->id_level_pengguna == 2)
                <a href="{{ route('rw-edit-datawarga', $item->id) }}" class="btn btn-primary btn-sm float-left"><i class="fa fa-pencil-alt"></i></a>
                <a href="" class="btn btn-danger btn-sm float-right"><i class="fa fa-trash"></i></a>
              @endif
              @if (Auth::user()->id_level_pengguna == 3)
                <a href="{{ route('rt-edit-datawarga', $item->id) }}" class="btn btn-primary btn-sm float-left"><i class="fa fa-pencil-alt"></i></a>
                <a href="" class="btn btn-danger btn-sm float-right"><i class="fa fa-trash"></i></a>
                @endif
          </td>
        </tr>
        @endforeach
      </tfoot>
    </table>
  </div>
  <!-- /.card-body -->
</div>
@endsection

@section('js')
<!-- DataTables  & Plugins -->
<script src="{{ asset('plugins/datatables/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('plugins/datatables-bs4/js/dataTables.bootstrap4.min.js') }}"></script>
<script src="{{ asset('plugins/datatables-responsive/js/dataTables.responsive.min.js') }}"></script>
<script src="{{ asset('plugins/datatables-responsive/js/responsive.bootstrap4.min.js') }}"></script>
<script src="{{ asset('plugins/datatables-buttons/js/dataTables.buttons.min.js') }}"></script>
<script src="{{ asset('plugins/datatables-buttons/js/buttons.bootstrap4.min.js') }}"></script>
<script src="{{ asset('plugins/jszip/jszip.min.js') }}"></script>
<script src="{{ asset('plugins/pdfmake/pdfmake.min.js') }}"></script>
<script src="{{ asset('plugins/pdfmake/vfs_fonts.js') }}"></script>
<script src="{{ asset('plugins/datatables-buttons/js/buttons.html5.min.js') }}"></script>
<script src="{{ asset('plugins/datatables-buttons/js/buttons.print.min.js') }}"></script>
<script src="{{ asset('plugins/datatables-buttons/js/buttons.colVis.min.js') }}"></script>

<script>
  $(function () {
    $("#example1").DataTable({
      "responsive": true, "lengthChange": false, "autoWidth": false,
      // "buttons": ["copy", "csv", "excel", "pdf", "print", "colvis"]
    }).buttons().container().appendTo('#example1_wrapper .col-md-6:eq(0)');
  });
</script>
@endsection