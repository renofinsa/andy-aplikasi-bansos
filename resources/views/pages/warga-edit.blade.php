@extends('layouts.master')

@section('title', 'Ubah Data Anggota | Aplikasi Bansos Warga')

@section('headline', 'Ubah Data Anggota')

@section('breadcrumb')
  <li class="breadcrumb-item"><a href="#">Kartu Keluarga</a></li>
  <li class="breadcrumb-item"><a href="#">Data Anggota Keluarga</a></li>
  <li class="breadcrumb-item active">Ubah</li>
@endsection

@section('content')
<div class="row justify-content-center">
  <div class="col-md-6 col-sm-12">
    <div class="card">
      <div class="card-header bg-dark">
      </div>
        @if (Auth::user()->id_level_pengguna == 1)
          <form action="{{ route('admin-update-datawarga', $data->id) }}" method="post">
        @endif
        @if (Auth::user()->id_level_pengguna == 2)
          <form action="{{ route('rw-update-datawarga', $data->id) }}" method="post">
        @endif
        @if (Auth::user()->id_level_pengguna == 3)
          <form action="{{ route('rt-update-datawarga', $data->id) }}" method="post">
        @endif
        @csrf
        <input type="hidden" name="_method" value="patch">
        <div class="card-body">
          <div class="form-group">
            <label for="id">Nomor Kartu Tanda Penduduk (KTP) <span class="text-danger">*</span></label>
            <input type="text" class="form-control @error('id') is-invalid @enderror" name="id" value="{{ $data->id }}" placeholder="Masukan Nomor KTP" disabled>
            @error('id')
              <span class="invalid-feedback" role="alert">
                <strong>{{ $message }}</strong>
              </span>
            @enderror
          </div>
          <div class="form-group">
            <label for="nama_lengkap">Nama Lengkap <span class="text-danger">*</span></label>
            <input type="text" class="form-control @error('nama_lengkap') is-invalid @enderror" name="nama_lengkap" value="{{ $data->nama_lengkap }}" placeholder="Masukan Nama Lengap">
            @error('nama_lengkap')
              <span class="invalid-feedback" role="alert">
                <strong>{{ $message }}</strong>
              </span>
            @enderror
          </div>
          <div class="form-group">
            <label for="jenis_kelamin">Jenis Kelamin <span class="text-danger">*</span></label>
            <div class="row pl-2">
              <div class="col-6 form-check">
                <input class="form-check-input" type="radio" name="jenis_kelamin" id="pria" value="1" {{ $data->jenis_kelamin == 1 ? 'checked' : ''}}>
                <label class="form-check-label" for="pria">
                  Pria
                </label>
              </div>
              <div class="col-6 form-check">
                <input class="form-check-input" type="radio" name="jenis_kelamin" id="wanita" value="0" {{ $data->jenis_kelamin == 0 ? 'checked' : ''}}>
                <label class="form-check-label" for="wanita">
                  Wanita
                </label>
              </div>
            </div>
            @error('jenis_kelamin')
              <span class="invalid-feedback" role="alert">
                <strong>{{ $message }}</strong>
              </span>
            @enderror
          </div>
          <div class="row">
            <div class="col-md-6 col-sm-12">
              <div class="form-group">
                <label for="tempat_lahir">Tempat Lahir <span class="text-danger">*</span></label>
                <input type="text" class="form-control @error('tempat_lahir') is-invalid @enderror" name="tempat_lahir" value="{{ $data->tempat_lahir }}" placeholder="Jakarta">
                @error('tempat_lahir')
                  <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                  </span>
                @enderror
              </div>
            </div>
            <div class="col-md-6 col-sm-12">
              <div class="form-group">
                <label for="tanggal_lahir">Tanggal Lahir <span class="text-danger">*</span></label>
                <input type="date" class="form-control @error('tanggal_lahir') is-invalid @enderror" name="tanggal_lahir" value="{{ $data->tanggal_lahir }}">
                @error('tanggal_lahir')
                  <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                  </span>
                @enderror
              </div>
            </div>
            <div class="col-md-6 col-sm-12">
              <div class="form-group">
                <label for="agama">Agama <span class="text-danger">*</span></label>
                <select name="agama" class="form-control @error('agama') is-invalid @enderror">
                  <option value="Islam" {{ $data->agama == 'Islam' ? 'selected' : ''}}>Islam</option>
                  <option value="Katolik" {{ $data->agama == 'Katolik' ? 'selected' : ''}}>Katolik</option>
                  <option value="Kristen" {{ $data->agama == 'Kristen' ? 'selected' : ''}}>Kristen</option>
                  <option value="Buddha" {{ $data->agama == 'Buddha' ? 'selected' : ''}}>Buddha</option>
                  <option value="Hindu" {{ $data->agama == 'Hindu' ? 'selected' : ''}}>Hindu</option>
                  <option value="Khonghucu" {{ $data->agama == 'Khonghucu' ? 'selected' : ''}}>Khonghucu</option>
                </select>
                @error('agama')
                  <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                  </span>
                @enderror
              </div>
            </div>
            <div class="col-md-6 col-sm-12">
              <div class="form-group">
                <label for="agama">Pendidikan Terakhir <span class="text-danger">*</span></label>
                <select name="pendidikan_terakhir" class="form-control @error('pendidikan_terakhir') is-invalid @enderror">
                  <option value="SD" {{ $data->pendidikan_terakhir == 'SD' ? 'selected' : ''}}>SD</option>
                  <option value="SMP" {{ $data->pendidikan_terakhir == 'SMP' ? 'selected' : ''}}>SMP</option>
                  <option value="SMA/SMK" {{ $data->pendidikan_terakhir == 'SMA/SMK' ? 'selected' : ''}}>SMA/SMK</option>
                  <option value="Belum Sekolah" {{ $data->pendidikan_terakhir == 'Belum Sekolah' ? 'selected' : ''}}>Belum Sekolah</option>
                  <option value="-" {{ $data->pendidikan_terakhir == '-' ? 'selected' : ''}}>-</option>
                </select>
                @error('pendidikan_terakhir')
                  <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                  </span>
                @enderror
              </div>
            </div>
            <div class="col-md-6 col-sm-12">
              <div class="form-group">
                <label for="agama">Pekerjaan <span class="text-danger">*</span></label>
                <select name="pekerjaan" class="form-control @error('pekerjaan') is-invalid @enderror">
                  <option value="Buruh" {{ $data->pekerjaan == 'Buruh' ? 'selected' : ''}}>Buruh</option>
                  <option value="Pegawai Negeri" {{ $data->pekerjaan == 'Pegawai Negeri' ? 'selected' : ''}}>Pegawai Negeri</option>
                  <option value="Pegawai Swasta" {{ $data->pekerjaan == 'Pegawai Swasta' ? 'selected' : ''}}>Pegawai Swasta</option>
                  <option value="Pelajar" {{ $data->pekerjaan == 'Pelajar' ? 'selected' : ''}}>Pelajar</option>
                  <option value="Ibu Rumah Tangga" {{ $data->pekerjaan == 'Ibu Rumah Tangga' ? 'selected' : ''}}>Ibu Rumah Tangga</option>
                </select>
                @error('pekerjaan')
                  <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                  </span>
                @enderror
              </div>
            </div>
            <div class="col-md-6 col-sm-12">
              <div class="form-group">
                <label for="golongan_darah">Golongan Darah <span class="text-danger">*</span></label>
                <select name="golongan_darah" class="form-control @error('golongan_darah') is-invalid @enderror">
                  <option value="A" {{ $data->golongan_darah == 'A' ? 'selected' : ''}}>A</option>
                  <option value="O" {{ $data->golongan_darah == 'O' ? 'selected' : ''}}>O</option>
                  <option value="B" {{ $data->golongan_darah == 'B' ? 'selected' : ''}}>B</option>
                  <option value="AB" {{ $data->golongan_darah == 'AB' ? 'selected' : ''}}>AB</option>
                  <option value="-" {{ $data->golongan_darah == '-' ? 'selected' : ''}}>-</option>
                </select>
                @error('golongan_darah')
                  <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                  </span>
                @enderror
              </div>
            </div>
            <div class="col-md-6 col-sm-12">
              <div class="form-group">
                <label for="status_perkawinan">Status Perkawinan <span class="text-danger">*</span></label>
                <select name="status_perkawinan" class="form-control @error('status_perkawinan') is-invalid @enderror">
                  <option value="Menikah" {{ $data->status_perkawinan == 'Menikah' ? 'selected' : ''}}>Menikah</option>
                  <option value="Kawin Tidak Tercatat" {{ $data->status_perkawinan == 'Kawin Tidak Tercatat' ? 'selected' : ''}}>Kawin Tidak Tercatat</option>
                  <option value="Belum Menikah" {{ $data->status_perkawinan == 'Belum Menikah' ? 'selected' : ''}}>Belum Menikah</option>
                  <option value="Duda" {{ $data->status_perkawinan == 'Duda' ? 'selected' : ''}}>Duda</option>
                  <option value="Janda" {{ $data->status_perkawinan == 'Janda' ? 'selected' : ''}}>Janda</option>
                  <option value="-" {{ $data->status_perkawinan == '-' ? 'selected' : ''}}>-</option>
                </select>
                @error('status_perkawinan')
                  <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                  </span>
                @enderror
              </div>
            </div>
            <div class="col-md-6 col-sm-12">
              <div class="form-group">
                <label for="status_keluarga">Status Dalam Keluarga <span class="text-danger">*</span></label>
                <select name="status_keluarga" class="form-control @error('status_keluarga') is-invalid @enderror">
                  <option value="Kepala Keluarga" {{$data->status_keluarga == 'Kepala Keluarga' ? 'selected' : ''}}>Kepala Keluarga</option>
                  <option value="Istri" {{$data->status_keluarga == 'Istri' ? 'selected' : ''}}>Istri</option>
                  <option value="Anak" {{$data->status_keluarga == 'Anak' ? 'selected' : ''}}>Anak</option>
                  <option value="Orang Tua" {{$data->status_keluarga == 'Orang Tua' ? 'selected' : ''}}>Orang Tua</option>
                  <option value="DLL" {{$data->status_keluarga == 'DLL' ? 'selected' : ''}}>DLL</option>
                </select>
                @error('status_keluarga')
                  <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                  </span>
                @enderror
              </div>
            </div>
          </div>
          <div class="form-group">
            <label for="wni">Status Kewarganegaraan <span class="text-danger">*</span></label>
            <div class="row pl-2">
              <div class="col-6 form-check">
                <input class="form-check-input" type="radio" name="wni" id="wni" value="1" {{ $data->wni == 1 ? 'checked' : '' }}>
                <label class="form-check-label" for="wni">
                  WNI
                </label>
              </div>
              <div class="col-6 form-check">
                <input class="form-check-input" type="radio" name="wni" id="wna" value="0" {{ $data->wni == 0 ? 'checked' : '' }}>
                <label class="form-check-label" for="wna">
                  WNA
                </label>
              </div>
            </div>
            @error('wni')
              <span class="invalid-feedback" role="alert">
                <strong>{{ $message }}</strong>
              </span>
            @enderror
          </div>
          <div class="form-group">
            <label for="nama_ayah">Nama Ayah <span class="text-danger">*</span></label>
            <input type="text" class="form-control @error('nama_ayah') is-invalid @enderror" name="nama_ayah" value="{{ $data->nama_ayah }}" placeholder="Masukan Nama Ayah">
            @error('nama_ayah')
              <span class="invalid-feedback" role="alert">
                <strong>{{ $message }}</strong>
              </span>
            @enderror
          </div>
          <div class="form-group">
            <label for="nama_ibu">Nama Ibu <span class="text-danger">*</span></label>
            <input type="text" class="form-control @error('nama_ibu') is-invalid @enderror" name="nama_ibu" value="{{ $data->nama_ibu }}" placeholder="Masukan Nama Ibu">
            @error('nama_ibu')
              <span class="invalid-feedback" role="alert">
                <strong>{{ $message }}</strong>
              </span>
            @enderror
          </div>
          <div class="form-group">
            <label for="id_level_pengguna">Level Pengguna <span class="text-danger">*</span></label>
            <select name="id_level_pengguna" class="form-control @error('id_level_pengguna') is-invalid @enderror">
              <option value="4" {{ $data->id_level_pengguna == 4 ? 'selected' : ''}}>Warga</option>
              <option value="2" {{ $data->id_level_pengguna == 2 ? 'selected' : ''}}>RW</option>
              <option value="3" {{ $data->id_level_pengguna == 3 ? 'selected' : ''}}>RT</option>
            </select>
            @error('id_level_pengguna')
              <span class="invalid-feedback" role="alert">
                <strong>{{ $message }}</strong>
              </span>
            @enderror
          </div>
          <div class="form-group">
            <button type="submit" class="btn btn-primary form-control btn-sm">Simpan Dan Ubah</button>
          </div>
        </div>
      </form>
      <!-- /.card-body -->
    </div>
  </div>
</div>
@endsection