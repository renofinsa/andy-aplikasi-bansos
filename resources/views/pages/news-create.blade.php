@extends('layouts.master')

@section('title', 'Berita | Aplikasi Bansos Warga')

@section('headline', 'Berita')

@section('css')
  <link rel="stylesheet" href="{{ asset('plugins/summernote/summernote-bs4.min.css') }}">
@endsection

@section('breadcrumb')
  <li class="breadcrumb-item"><a href="#">Berita</a></li>
  <li class="breadcrumb-item active">Baru</li>
@endsection

@section('content')
<div class="row justify-content-center">
  <div class="col-md-6 col-sm-12">
    <div class="card">
      <div class="card-header bg-dark">
      </div>
      <!-- /.card-header -->
      @if (Auth::user()->id_level_pengguna == 1)
        <form action="{{ route('admin-news-store') }}" method="post">
      @endif
      @if (Auth::user()->id_level_pengguna == 2)
        <form action="{{ route('rw-news-store') }}" method="post">
      @endif
      @if (Auth::user()->id_level_pengguna == 3)
        <form action="{{ route('rt-news-store') }}" method="post">
      @endif

        @csrf
        <div class="card-body">
          <div class="form-group">
            <label for="judul">Judul <span class="text-danger">*</span></label>
            <input type="text" class="form-control @error('judul') is-invalid @enderror" name="judul" value="{{ old('judul') }}" placeholder="Masukan Judul Berita" >
            @error('judul')
              <span class="invalid-feedback" role="alert">
                <strong>{{ $message }}</strong>
              </span>
            @enderror
          </div>
          <div class="form-group">
            <label for="deskripsi">Isi Berita <span class="text-danger">*</span></label>
            <textarea id="summernote" name="deskripsi" cols="30" rows="20" class="form-control @error('deskripsi') is-invalid @enderror" placeholder="Jl permai raya no 19">{{ old('deskripsi') }}</textarea>
            @error('deskripsi')
              <span class="invalid-feedback" role="alert">
                <strong>{{ $message }}</strong>
              </span>
            @enderror
          </div>
          <div class="form-group">
            <button type="submit" class="btn btn-primary form-control btn-sm">Buat Berita</button>
          </div>
        </div>
      </form>
      <!-- /.card-body -->
    </div>
  </div>
</div>
@endsection

@section('js')
<script src="{{ asset('plugins/summernote/summernote-bs4.min.js') }}"></script>
<script>
  $(function () {
    // Summernote
    $('#summernote').summernote()
  })
</script>
@endsection