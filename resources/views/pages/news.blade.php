@extends('layouts.master')

@section('title', 'Berita | Aplikasi Bansos Warga')

@section('css')
  <!-- DataTables -->
  <link rel="stylesheet" href="{{ asset('plugins/datatables-bs4/css/dataTables.bootstrap4.min.css') }}">
  <link rel="stylesheet" href="{{ asset('plugins/datatables-responsive/css/responsive.bootstrap4.min.css') }}">
  <link rel="stylesheet" href="{{ asset('plugins/datatables-buttons/css/buttons.bootstrap4.min.css') }}">
@endsection

@section('headline', 'Berita')

@section('breadcrumb')
  <li class="breadcrumb-item">Berita</li>
@endsection

@section('content')
<div class="card">
  <div class="card-header bg-dark">
  </div>
  <!-- /.card-header -->
  <div class="card-body">
    @if (Auth::user()->id_level_pengguna == 1)
      <a href="{{ route('admin-news-create') }}" class="btn btn-success btn-sm float-left"><i class="fa fa-plus"></i> Tambah Berita</a>
    @endif
    @if (Auth::user()->id_level_pengguna == 2)
      <a href="{{ route('rw-news-create') }}" class="btn btn-success btn-sm float-left"><i class="fa fa-plus"></i> Tambah Berita</a>
    @endif
    @if (Auth::user()->id_level_pengguna == 3)
      <a href="{{ route('rt-news-create') }}" class="btn btn-success btn-sm float-left"><i class="fa fa-plus"></i> Tambah Berita</a>
    @endif

    <table id="example1" class="table table-bordered table-striped">
      <thead>
      <tr>
        <th>No.</th>
        <th>Judul</th>
        <th>Tanggal Buat</th>
        <th>Pembuat</th>
        <th width="80">Aksi</th>
      </tr>
      </thead>
      <tbody>
        @foreach ($data as $key=>$item)
        <tr>
          <td>{{ $key+1 }}</td>
          <td>{{ $item->judul }}</td>
          <td>{{ date('d M Y', strtotime($item->tanggal_buat)) }}</td>
          <td>{{ $item->author->nama_lengkap }}</td>
          <td>
            @if (Auth::user()->id_level_pengguna == 1)
              <a href="{{ route('admin-news-edit', $item->id) }}" class="btn btn-primary btn-sm float-left ml-1 mr-1"><i class="fa fa-pencil-alt"></i></a>
              <a href="{{ route('admin-news-destroy', $item->id) }}" class="btn btn-danger btn-sm float-left ml-1 mr-1"><i class="fa fa-trash"></i></a>
            @endif
            @if (Auth::user()->id_level_pengguna == 2)
              <a href="{{ route('rw-news-edit', $item->id) }}" class="btn btn-primary btn-sm float-left ml-1 mr-1"><i class="fa fa-pencil-alt"></i></a>
              <a href="{{ route('rt-news-destroy', $item->id) }}" class="btn btn-danger btn-sm float-left ml-1 mr-1"><i class="fa fa-trash"></i></a>
            @endif
            @if (Auth::user()->id_level_pengguna == 3)
              <a href="{{ route('rt-news-edit', $item->id) }}" class="btn btn-primary btn-sm float-left ml-1 mr-1"><i class="fa fa-pencil-alt"></i></a>
              <a href="{{ route('rt-news-destroy', $item->id) }}" class="btn btn-danger btn-sm float-left ml-1 mr-1"><i class="fa fa-trash"></i></a>
            @endif
          </td>
        </tr>
        @endforeach
      </tfoot>
    </table>
  </div>
  <!-- /.card-body -->
</div>
@endsection

@section('js')
<!-- DataTables  & Plugins -->
<script src="{{ asset('plugins/datatables/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('plugins/datatables-bs4/js/dataTables.bootstrap4.min.js') }}"></script>
<script src="{{ asset('plugins/datatables-responsive/js/dataTables.responsive.min.js') }}"></script>
<script src="{{ asset('plugins/datatables-responsive/js/responsive.bootstrap4.min.js') }}"></script>
<script src="{{ asset('plugins/datatables-buttons/js/dataTables.buttons.min.js') }}"></script>
<script src="{{ asset('plugins/datatables-buttons/js/buttons.bootstrap4.min.js') }}"></script>
<script src="{{ asset('plugins/jszip/jszip.min.js') }}"></script>
<script src="{{ asset('plugins/pdfmake/pdfmake.min.js') }}"></script>
<script src="{{ asset('plugins/pdfmake/vfs_fonts.js') }}"></script>
<script src="{{ asset('plugins/datatables-buttons/js/buttons.html5.min.js') }}"></script>
<script src="{{ asset('plugins/datatables-buttons/js/buttons.print.min.js') }}"></script>
<script src="{{ asset('plugins/datatables-buttons/js/buttons.colVis.min.js') }}"></script>

<script>
  $(function () {
    $("#example1").DataTable({
      "responsive": true, "lengthChange": false, "autoWidth": false,
      // "buttons": ["copy", "csv", "excel", "pdf", "print", "colvis"]
    }).buttons().container().appendTo('#example1_wrapper .col-md-6:eq(0)');
  });
</script>
@endsection