@extends('layouts.master')

@section('title', 'Pengajuan Warga | Aplikasi Bansos Warga')

@section('css')
  <!-- DataTables -->
  <link rel="stylesheet" href="{{ asset('plugins/datatables-bs4/css/dataTables.bootstrap4.min.css') }}">
  <link rel="stylesheet" href="{{ asset('plugins/datatables-responsive/css/responsive.bootstrap4.min.css') }}">
  <link rel="stylesheet" href="{{ asset('plugins/datatables-buttons/css/buttons.bootstrap4.min.css') }}">
@endsection

@section('headline', 'Pengajuan Warga')

@section('breadcrumb')
  <li class="breadcrumb-item active">Pengajuan Warga</li>
@endsection

@section('content')
<div class="card">
  <div class="card-header bg-dark">
  </div>
  <!-- /.card-header -->
  <div class="card-body">
      @if (Auth::user()->id_level_pengguna == 2)
      <a href="{{ route('rw-pengajuan-surat-pengantar') }}" class="btn btn-success btn-sm float-left ml-2 mr-2"><i class="fa fa-plus"></i> Surat Pengantar</a>
      <a href="{{ route('rw-pengajuan-surat-keterangan-tidak-mampu') }}" class="btn btn-primary btn-sm float-left ml-2 mr-2"><i class="fa fa-plus"></i> Surat Keterangan Tidak Mampu</a>
    @endif
    @if (Auth::user()->id_level_pengguna == 3)
      <a href="{{ route('rt-pengajuan-surat-pengantar') }}" class="btn btn-success btn-sm float-left ml-2 mr-2"><i class="fa fa-plus"></i> Surat Pengantar</a>
      <a href="{{ route('rt-pengajuan-surat-keterangan-tidak-mampu') }}" class="btn btn-primary btn-sm float-left ml-2 mr-2"><i class="fa fa-plus"></i> Surat Keterangan Tidak Mampu</a>
    @endif
    @if (Auth::user()->id_level_pengguna == 4)
    <a href="{{ route('warga-pengajuan-surat-pengantar') }}" class="btn btn-success btn-sm float-left ml-2 mr-2"><i class="fa fa-plus"></i> Surat Pengantar</a>
    <a href="{{ route('warga-pengajuan-surat-keterangan-tidak-mampu') }}" class="btn btn-primary btn-sm float-left ml-2 mr-2"><i class="fa fa-plus"></i> Surat Keterangan Tidak Mampu</a>
    @endif
    <table id="example1" class="table table-bordered table-striped">
      <thead>
      <tr>
        <th>No.</th>
        <th>Jenis Pengajuan</th>
        <th>Tanggal Buat</th>
        <th>Status</th>
      </tr>
      </thead>
      <tbody>
        @foreach ($data as $key=>$item)
        <tr>
          <td>{{ $key+1 }}</td>
          <td>{{ $item->jenis->nama }}</td>
          <td>{{ date('d M Y', strtotime($item->tanggal_pengajuan)) }}</td>
          <td style="text-transform: uppercase">{{ $item->status }}
            @if ($item->status == 'diterima')
                <a target="_blank" href="{{ route('export-surat-pengantar', $item->id) }}" class="btn btn-success btn-sm"><i class="fa fa-download"></i> Unduh</a>
            @endif
          </td>
        </tr>
        @endforeach
      </tfoot>
    </table>
  </div>
  <!-- /.card-body -->
</div>
@endsection

@section('js')
<!-- DataTables  & Plugins -->
<script src="{{ asset('plugins/datatables/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('plugins/datatables-bs4/js/dataTables.bootstrap4.min.js') }}"></script>
<script src="{{ asset('plugins/datatables-responsive/js/dataTables.responsive.min.js') }}"></script>
<script src="{{ asset('plugins/datatables-responsive/js/responsive.bootstrap4.min.js') }}"></script>
<script src="{{ asset('plugins/datatables-buttons/js/dataTables.buttons.min.js') }}"></script>
<script src="{{ asset('plugins/datatables-buttons/js/buttons.bootstrap4.min.js') }}"></script>
<script src="{{ asset('plugins/jszip/jszip.min.js') }}"></script>
<script src="{{ asset('plugins/pdfmake/pdfmake.min.js') }}"></script>
<script src="{{ asset('plugins/pdfmake/vfs_fonts.js') }}"></script>
<script src="{{ asset('plugins/datatables-buttons/js/buttons.html5.min.js') }}"></script>
<script src="{{ asset('plugins/datatables-buttons/js/buttons.print.min.js') }}"></script>
<script src="{{ asset('plugins/datatables-buttons/js/buttons.colVis.min.js') }}"></script>

<script>
  $(function () {
    $("#example1").DataTable({
      "responsive": true, "lengthChange": false, "autoWidth": false,
      // "buttons": ["copy", "csv", "excel", "pdf", "print", "colvis"]
    }).buttons().container().appendTo('#example1_wrapper .col-md-6:eq(0)');
  });
</script>
@endsection